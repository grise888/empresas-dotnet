using BR.Ioasys.IMDB.API.Domain.Aggregates;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BR.Ioasys.IMDB.API.Domain.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : EntityCore<TEntity>
    {
        TEntity Add(TEntity entity);
        void Add(IEnumerable<TEntity> entity);
        void AddRange(List<TEntity> entity);
        TEntity GetById(params object[] ids);
        IEnumerable<TEntity> GetAll();
        TEntity Update(TEntity entity);
        void Update(IEnumerable<TEntity> entity);
        void UpdateRange(List<TEntity> entity);
        void Update(TEntity current, TEntity updated);
        TEntity Remove(params object[] ids);
        TEntity Remove(TEntity entity);
        void Remove(IEnumerable<TEntity> entity);
        void RemoveRange(List<TEntity> entity);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        int SaveChanges();
    }
}
