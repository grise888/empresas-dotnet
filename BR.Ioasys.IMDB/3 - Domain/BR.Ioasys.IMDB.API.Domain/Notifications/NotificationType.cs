using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Notifications
{
    public enum NotificationType
    {
        Information,
        Error
    }
}
