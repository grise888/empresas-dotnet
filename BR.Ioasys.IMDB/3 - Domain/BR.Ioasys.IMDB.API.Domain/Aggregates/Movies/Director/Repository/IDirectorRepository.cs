using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
    public interface IDirectorRepository : IRepository<Director>
    {
		
    }
}