namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
	
	public class Director : EntityCore<Director>
	{  
	    
		public int DirectorId { get; set; }
		
		public string Name { get; set; }
		
		public List<Movie> MovieList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}