using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Services
{
    public interface IDirectorService : IDisposable
    {
        
		public Director InsertDirector(Director director);

		public Director UpdateDirector(Director director);

		public Director DeleteDirector(int directorId);

		public Director GetDirector(int directorId);

		public List<Director> GetDirectors();

    }
}
