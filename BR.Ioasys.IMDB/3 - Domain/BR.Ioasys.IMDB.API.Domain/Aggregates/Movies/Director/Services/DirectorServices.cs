using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Services
{
    public class DirectorService : Service, IDirectorService
    {

        private readonly IDirectorRepository _directorRepository;

        public DirectorService(IMediator mediator,
                              IDirectorRepository directorRepository
                              ) : base(mediator)
        {
            _directorRepository = directorRepository;
        }
        
		public Director InsertDirector(Director director)
		{
			_directorRepository.Add(director);
			_directorRepository.SaveChanges();
			return director;
		}

		public Director UpdateDirector(Director director)
		{
			_directorRepository.Update(director);
			_directorRepository.SaveChanges();
			return director;
		}

		public Director DeleteDirector(int directorId)
		{
			Director director = _directorRepository.Remove(directorId);
			_directorRepository.SaveChanges();
			return director;
		}

		public Director GetDirector(int directorId)
		{
			 return _directorRepository.Find(x => x.DirectorId == directorId).FirstOrDefault();
		}

		public List<Director> GetDirectors()
		{
			 return _directorRepository.GetAll().ToList();
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _directorRepository.Dispose();
        }

    }
}