﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Core.Security;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login.Services
{
    public class LoginService : Service, ILoginService
    {

        private readonly IUserRepository _userRepository;
        public SigningConfigurations _signingConfigurations;
        public TokenConfigurations _tokenConfigurations;

        public LoginService(IMediator mediator,
                            SigningConfigurations signingConfigurations,
                            TokenConfigurations tokenConfigurations,
                            IUserRepository userRepository
                           ) : base(mediator)
        {
            _userRepository = userRepository;
            _signingConfigurations = signingConfigurations;
            _tokenConfigurations = tokenConfigurations;
        }
        public UserLogin Signin(string login, string password)
        {
            UserLogin userLogin = _userRepository.Signin(login, password);
            if (userLogin != null)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                     new GenericIdentity(login),
                     new[]
                     {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                         new Claim(JwtRegisteredClaimNames.UniqueName, login),
                     }
                );
                DateTime createDate = DateTime.UtcNow;
                DateTime expirationDate = createDate + TimeSpan.FromSeconds(_tokenConfigurations.Seconds);
                var handler = new JwtSecurityTokenHandler();
                string token = CreateToken(identity, createDate, expirationDate, handler);
                userLogin.Token = token;
            }

            return userLogin;
        }
        private string CreateToken(ClaimsIdentity identity, DateTime createDate, DateTime expirationDate, JwtSecurityTokenHandler handler)
        {
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = createDate.AddSeconds(1),
                Expires = expirationDate.Date.AddDays(4),
            });

            var token = handler.WriteToken(securityToken);
            return token;
        }
    }
}
