﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login.Services
{
    public interface ILoginService
    {
        public UserLogin Signin(string login, string password);
    }
}
