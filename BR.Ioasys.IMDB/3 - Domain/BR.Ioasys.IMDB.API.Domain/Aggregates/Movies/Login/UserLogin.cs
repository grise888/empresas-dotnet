﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login
{
    public class UserLogin
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Profile { get; set; }
        public string Token { get; set; }
    }
}
