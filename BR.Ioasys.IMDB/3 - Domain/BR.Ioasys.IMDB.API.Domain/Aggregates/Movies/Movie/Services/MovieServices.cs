using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Repository;

using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Services
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
	public class MovieService : Service, IMovieService
    {

        private readonly IMovieRepository _movieRepository;
		private readonly IMovieActorsRepository _movieActorsRepository;
		private readonly IMovieNotesRepository __movieNotesRepository;

		public MovieService(IMediator mediator,
                            IMovieRepository movieRepository,
							IMovieActorsRepository movieActorsRepository,
							IMovieNotesRepository _movieNotesRepository
							  ) : base(mediator)
        {
            _movieRepository = movieRepository;
			_movieActorsRepository = movieActorsRepository;
			__movieNotesRepository = _movieNotesRepository;

		}
        
		public Movie InsertMovie(Movie movie)
		{
			_movieRepository.Add(movie);
			_movieRepository.SaveChanges();
			return movie;
		}

		public Movie UpdateMovie(Movie movie)
		{
			_movieRepository.Update(movie);
			_movieRepository.SaveChanges();
			return movie;
		}

		public Movie DeleteMovie(int movieId)
		{
		     var movie = _movieRepository.Remove(movieId);
			_movieRepository.SaveChanges();
			return movie;
		}

		public Movie GetMovie(int movieId)
		{
			 return _movieRepository.Find(x => x.MovieId == movieId).FirstOrDefault();
		}

		public List<Movie> GetMovies()
		{
			 return _movieRepository.GetAll().ToList();
		}

		public List<Movie> GetMovieByGender(int genderId)
		{
			 return _movieRepository.GetMovieByGender(genderId);
		}

		public List<Movie> GetMovieByDirector(int directorId)
		{
			 return _movieRepository.GetMovieByDirector(directorId);
		}

		public Movie DetailMovie(int movieId)
		{
			List<Movie> movies = GetMoviesByFilter(new Movie());
			return movies.Where(x => x.MovieId == movieId).FirstOrDefault(); ;
		}

		public List<Movie> GetMoviesByFilter(Movie movie)
		{
			List<Movie> movies = new List<Movie>();
			List<Movie> movieList =  _movieRepository.GetMoviesByFilter(movie).ToList();
			foreach (var mov in movieList)
			{
				movie.NoteAverage = CalculateAverage(mov.MovieId);
				var actors = _movieActorsRepository.GetMovieActorsByMovie(mov.MovieId).Select(x => x.Actor).ToList();
				mov.ActorList = actors;
				if (movie.ActorName == null) movies.Add(mov);
				if (GetMovieActors(mov.MovieId, movie.ActorName))
				{
					
					movies.Add(mov);
				}
			}
			return movies.OrderBy(x=> x.Name).ThenBy(x=> x.NoteAverage).ToList();
		}





		private bool GetMovieActors(int movieId, string actionName)
        {
			var actors = _movieActorsRepository.GetMovieActorsByMovie(movieId).Select(x=> x.Actor).ToList();
			if (actionName != null)
			{
				var searchActors = actors.Where(x => x.Name.Contains(actionName)).ToList();
				if (searchActors.Count > 0)
				{
					return true;
				}
			}

			return false;

		}
		private double  CalculateAverage(int movieId)
		{
			List<MovieNotes> Notes = __movieNotesRepository.GetMovieNotesByMovie(movieId).ToList();
			int total = Notes.Count;
			int sum = Notes.Sum(x => x.Note);
			if (total == 0) return 0.0;
			return sum / total;
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _movieRepository.Dispose();
        }

    }
}