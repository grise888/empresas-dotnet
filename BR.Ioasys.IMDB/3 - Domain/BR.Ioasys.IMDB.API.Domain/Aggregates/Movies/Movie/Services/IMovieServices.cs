using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Services
{
    public interface IMovieService : IDisposable
    {
    
		public Movie InsertMovie(Movie movie);
		public Movie UpdateMovie(Movie movie);
		public Movie DeleteMovie(int movieId);
		public Movie GetMovie(int movieId);
		public List<Movie> GetMovies();
		public List<Movie> GetMovieByGender(int genderId);
		public List<Movie> GetMovieByDirector(int directorId);
		public List<Movie> GetMoviesByFilter(Movie movie);
		public Movie DetailMovie(int movieId);

	}
}
