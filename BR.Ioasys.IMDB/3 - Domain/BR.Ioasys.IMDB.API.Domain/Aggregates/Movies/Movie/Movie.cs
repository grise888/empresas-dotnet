namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie
{
	using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
	
	public class Movie : EntityCore<Movie>
	{  
	    
		public int MovieId { get; set; }
		
		public string Name { get; set; }
		public string Detail { get; set; }
		public int GenderId { get; set; }
		[NotMapped]
		public string GenderName { get; set; }
		
		public Gender Gender { get; set; }
		public int DirectorId { get; set; }

		[NotMapped]
		public string DirectorName { get; set; }
		public Director Director { get; set; }
	    public List<MovieActors> MovieActorsList { get; set; }
		public List<MovieNotes> MovieNotesList { get; set; }

		[NotMapped]
		public string ActorName { get; set; }

		[NotMapped]
		public List<Actor> ActorList { get; set; }

		[NotMapped]
		public double NoteAverage { get; set; }

		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}