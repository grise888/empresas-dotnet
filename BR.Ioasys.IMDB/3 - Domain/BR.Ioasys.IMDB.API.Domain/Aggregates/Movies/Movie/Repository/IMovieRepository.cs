using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
    public interface IMovieRepository : IRepository<Movie>
    {
		
		public List<Movie> GetMovieByGender(int genderId);

		public List<Movie> GetMovieByDirector(int directorId);

		public List<Movie> GetMoviesByFilter(Movie movie);

    }
}