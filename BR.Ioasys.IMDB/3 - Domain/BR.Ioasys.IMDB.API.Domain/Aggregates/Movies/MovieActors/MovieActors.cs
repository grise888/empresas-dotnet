namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
	
	public class MovieActors : EntityCore<MovieActors>
	{  
	    
		public int MovieActorId { get; set; }
		public int ActorId { get; set; }

		public Actor Actor { get; set; }
		public int MovieId { get; set; }

		public Movie Movie { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}