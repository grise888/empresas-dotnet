using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Services
{
    public interface IMovieActorsService : IDisposable
    {
        
		public void InsertMovieActors(MovieActors movieActors);

		public void UpdateMovieActors(MovieActors movieActors);

		public void DeleteMovieActors(int movieActorId);

		public MovieActors GetMovieActors(int movieActorId);

		public List<MovieActors> GetMoviesActors();

		public List<MovieActors> GetMovieActorsByActor(int movieId);

		public List<MovieActors> GetMovieActorsByMovie(int actorId);

    }
}
