using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Services
{
    public class MovieActorsService : Service, IMovieActorsService
    {

        private readonly IMovieActorsRepository _movieActorsRepository;

        public MovieActorsService(IMediator mediator,
                              IMovieActorsRepository movieActorsRepository
                              ) : base(mediator)
        {
            _movieActorsRepository = movieActorsRepository;
        }
        
		public void InsertMovieActors(MovieActors movieActors)
		{
			_movieActorsRepository.Add(movieActors);
			_movieActorsRepository.SaveChanges();
		}

		public void UpdateMovieActors(MovieActors movieActors)
		{
			_movieActorsRepository.Update(movieActors);
			_movieActorsRepository.SaveChanges();
		}

		public void DeleteMovieActors(int movieActorId)
		{
			_movieActorsRepository.Remove(movieActorId);
			_movieActorsRepository.SaveChanges();
		}

		public MovieActors GetMovieActors(int movieActorId)
		{
			 return _movieActorsRepository.Find(x => x.MovieActorId == movieActorId).FirstOrDefault();
		}

		public List<MovieActors> GetMoviesActors()
		{
			 return _movieActorsRepository.GetAll().ToList();
		}

		public List<MovieActors> GetMovieActorsByActor(int actorId)
		{
			 return _movieActorsRepository.GetMovieActorsByActor(actorId);
		}

		public List<MovieActors> GetMovieActorsByMovie(int movieId)
		{
			 return _movieActorsRepository.GetMovieActorsByMovie(movieId);
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _movieActorsRepository.Dispose();
        }

    }
}