using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
    public interface IMovieActorsRepository : IRepository<MovieActors>
    {
		
		public List<MovieActors> GetMovieActorsByActor(int actorId);

		public List<MovieActors> GetMovieActorsByMovie(int movieId);

    }
}