namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
	
	public class UserProfile : EntityCore<UserProfile>
	{  
	    
		public int ProfileId { get; set; }
		
		public string Name { get; set; }
		
		public List<User> UserList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}