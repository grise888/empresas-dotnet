using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Services
{
    public class UserProfileService : Service, IUserProfileService
    {

        private readonly IUserProfileRepository _userProfileRepository;

        public UserProfileService(IMediator mediator,
                              IUserProfileRepository userProfileRepository
                              ) : base(mediator)
        {
            _userProfileRepository = userProfileRepository;
        }
        
		public UserProfile InsertUserProfile(UserProfile userProfile)
		{
			_userProfileRepository.Add(userProfile);
			_userProfileRepository.SaveChanges();
			return userProfile;
		}

		public UserProfile UpdateUserProfile(UserProfile userProfile)
		{
			_userProfileRepository.Update(userProfile);
			_userProfileRepository.SaveChanges();
			return userProfile;
		}

		public UserProfile DeleteUserProfile(int profileId)
		{
			UserProfile userProfile = _userProfileRepository.Remove(profileId);
			_userProfileRepository.SaveChanges();
			return userProfile;
		}

		public UserProfile GetUserProfile(int profileId)
		{
			 return _userProfileRepository.Find(x => x.ProfileId == profileId).FirstOrDefault();
		}

		public List<UserProfile> GetProfiles()
		{
			 return _userProfileRepository.GetAll().ToList();
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _userProfileRepository.Dispose();
        }

    }
}