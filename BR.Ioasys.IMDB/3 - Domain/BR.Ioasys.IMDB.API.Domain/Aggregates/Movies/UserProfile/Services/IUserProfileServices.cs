using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Services
{
    public interface IUserProfileService : IDisposable
    {
        
		public UserProfile InsertUserProfile(UserProfile userProfile);

		public UserProfile UpdateUserProfile(UserProfile userProfile);

		public UserProfile DeleteUserProfile(int profileId);

		public UserProfile GetUserProfile(int profileId);

		public List<UserProfile> GetProfiles();

    }
}
