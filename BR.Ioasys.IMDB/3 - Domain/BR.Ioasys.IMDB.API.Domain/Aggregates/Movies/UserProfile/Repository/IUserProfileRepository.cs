using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
		
    }
}