﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User
{
    public enum  Status
    {
        None = 0,
        Ativo = 1,
        Inativo = 2
    }
}
