namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
	
	public class User : EntityCore<User>
	{  
	    
		public int UserId { get; set; }
		
		public string Name { get; set; }
		
		public string Email { get; set; }
		
		public string Login { get; set; }
		
		public string Password { get; set; }
		
		public int StatusId { get; set; }
		
		public UserStatus UserStatus { get; set; }
		
		public int ProfileId { get; set; }
		
		public UserProfile UserProfile { get; set; }
		
		public List<MovieNotes> MovieNotesList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}