﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User
{
    public enum ProfileEnum
    {
        None = 0,
        Administrador = 1,
        Cliente = 2
    }
}
