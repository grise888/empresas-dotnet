﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User
{
    public class UserFilter
    {
        public bool Pagination { get; set; }
        public int Page { get; set; }
        public int NumberRecords { get; set; }
    }
}
