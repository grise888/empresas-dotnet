using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository
{
    using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login;
    using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
    public interface IUserRepository : IRepository<User>
    {
		
		public List<User> GetUserByUserProfile(int profileId);

		public List<User> GetUserByUserStatus(int statusId);

		public List<User> GetUsersByFilter(User user);

		public List<User> GetActiveClients(UserFilter filter);

		public UserLogin Signin(string login, string password);

	}
}