using BR.Ioasys.IMDB.API.Domain.Aggregates.Core.Security;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Services
{
    public class UserService : Service, IUserService
    {

        private readonly IUserRepository _userRepository;

		public UserService(IMediator mediator,
						   IUserRepository userRepository
                              ) : base(mediator)
        {
            _userRepository = userRepository;
		}
        
		public User InsertUser(User user)
		{
			_userRepository.Add(user);
			_userRepository.SaveChanges();
			return user;
		}

		public User UpdateUser(User user)
		{
			_userRepository.Update(user);
			_userRepository.SaveChanges();
			return user;
		}

		public User DeleteUser(int userId)
		{
			var user = _userRepository.Remove(userId);
			_userRepository.SaveChanges();
			return user;
		}

		public User InactivateUser(int userId)
		{
			User user = _userRepository.GetById(userId);
			user.StatusId = (int) Status.Inativo;
			_userRepository.Update(user);
			_userRepository.SaveChanges();
			return user;
		}

		public User ActivateUser(int userId)
		{
			User user = _userRepository.GetById(userId);
			user.StatusId = (int) Status.Ativo;
			_userRepository.Update(user);
			_userRepository.SaveChanges();
			return user;
		}

		public User GetUser(int userId)
		{
			 return _userRepository.Find(x => x.UserId == userId).FirstOrDefault();
		}

		public List<User> GetUsers()
		{
			 return _userRepository.GetAll().ToList();
		}

		public List<User> GetUserByUserProfile(int profileId)
		{
			 return _userRepository.GetUserByUserProfile(profileId);
		}

		public List<User> GetUserByUserStatus(int statusId)
		{
			 return _userRepository.GetUserByUserStatus(statusId);
		}

		public List<User> GetUsersByFilter(User user)
		{
			 return _userRepository.GetAll().ToList();
		}

		public List<User> GetActiveClients(UserFilter filter)
		{
			return _userRepository.GetActiveClients(filter);
		}

		public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _userRepository.Dispose();
        }

    }
}