using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Services
{
    public interface IUserService : IDisposable
    {
        
		public User InsertUser(User user);

		public User UpdateUser(User user);

		public User DeleteUser(int userId);

		public User ActivateUser(int userId);

		public User InactivateUser(int userId);

		public User GetUser(int userId);

		public List<User> GetUsers();

		public List<User> GetUserByUserProfile(int profileId);
		public List<User> GetUserByUserStatus(int statusId);
		public List<User> GetUsersByFilter(User user);

		public List<User> GetActiveClients(UserFilter filter);

	}
}
