using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
    public interface IGenderRepository : IRepository<Gender>
    {
		
    }
}