using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Services
{
    public interface IGenderService : IDisposable
    {
        
		public Gender InsertGender(Gender gender);

		public Gender UpdateGender(Gender gender);

		public Gender DeleteGender(int genderId);

		public Gender GetGender(int genderId);

		public List<Gender> GetGenres();

    }
}
