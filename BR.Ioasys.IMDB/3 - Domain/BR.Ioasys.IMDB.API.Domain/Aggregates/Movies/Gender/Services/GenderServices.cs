using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Services
{
    public class GenderService : Service, IGenderService
    {

        private readonly IGenderRepository _genderRepository;

        public GenderService(IMediator mediator,
                              IGenderRepository genderRepository
                              ) : base(mediator)
        {
            _genderRepository = genderRepository;
        }
        
		public Gender InsertGender(Gender gender)
		{
			_genderRepository.Add(gender);
			_genderRepository.SaveChanges();
			return gender;
		}

		public Gender UpdateGender(Gender gender)
		{
			_genderRepository.Update(gender);
			_genderRepository.SaveChanges();
			return gender;
		}

		public Gender DeleteGender(int genderId)
		{
			Gender gender = _genderRepository.Remove(genderId);
			_genderRepository.SaveChanges();
			return gender;
		}

		public Gender GetGender(int genderId)
		{
			 return _genderRepository.Find(x => x.GenderId == genderId).FirstOrDefault();
		}

		public List<Gender> GetGenres()
		{
			 return _genderRepository.GetAll().ToList();
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _genderRepository.Dispose();
        }

    }
}