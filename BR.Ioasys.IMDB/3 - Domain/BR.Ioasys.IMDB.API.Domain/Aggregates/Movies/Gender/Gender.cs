namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
	
	public class Gender : EntityCore<Gender>
	{  
	    
		public int GenderId { get; set; }
		
		public string Name { get; set; }
		
		public List<Movie> MovieList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}