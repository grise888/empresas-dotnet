namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
	
	public class UserStatus : EntityCore<UserStatus>
	{  
	    
		public int StatusId { get; set; }
		
		public string Name { get; set; }
		
		public List<User> UserList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}