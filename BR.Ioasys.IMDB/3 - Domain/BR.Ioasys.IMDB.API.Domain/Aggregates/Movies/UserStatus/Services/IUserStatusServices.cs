using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Services
{
    public interface IUserStatusService : IDisposable
    {
        
		public UserStatus InsertUserStatus(UserStatus userStatus);

		public UserStatus UpdateUserStatus(UserStatus userStatus);

		public UserStatus DeleteUserStatus(int statusId);

		public UserStatus GetUserStatus(int statusId);

		public List<UserStatus> GetUsersStatus();

    }
}
