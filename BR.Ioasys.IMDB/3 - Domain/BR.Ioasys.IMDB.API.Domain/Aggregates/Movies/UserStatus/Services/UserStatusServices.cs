using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Services
{
    public class UserStatusService : Service, IUserStatusService
    {

        private readonly IUserStatusRepository _userStatusRepository;

        public UserStatusService(IMediator mediator,
                              IUserStatusRepository userStatusRepository
                              ) : base(mediator)
        {
            _userStatusRepository = userStatusRepository;
        }
        
		public UserStatus InsertUserStatus(UserStatus userStatus)
		{
			_userStatusRepository.Add(userStatus);
			_userStatusRepository.SaveChanges();
			return userStatus;
		}

		public UserStatus UpdateUserStatus(UserStatus userStatus)
		{
			_userStatusRepository.Update(userStatus);
			_userStatusRepository.SaveChanges();
			return userStatus;
		}

		public UserStatus DeleteUserStatus(int statusId)
		{
			UserStatus userStatus = _userStatusRepository.Remove(statusId);
			_userStatusRepository.SaveChanges();
			return userStatus;
		}

		public UserStatus GetUserStatus(int statusId)
		{
			 return _userStatusRepository.Find(x => x.StatusId == statusId).FirstOrDefault();
		}

		public List<UserStatus> GetUsersStatus()
		{
			 return _userStatusRepository.GetAll().ToList();
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _userStatusRepository.Dispose();
        }

    }
}