namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
	
	public class Actor : EntityCore<Actor>
	{  
	    
		public int ActorId { get; set; }
		
		public string Name { get; set; }
		
		public List<MovieActors> MovieActorsList { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}