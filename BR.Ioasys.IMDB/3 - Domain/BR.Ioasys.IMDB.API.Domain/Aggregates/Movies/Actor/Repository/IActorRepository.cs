using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
    public interface IActorRepository : IRepository<Actor>
    {
		
    }
}