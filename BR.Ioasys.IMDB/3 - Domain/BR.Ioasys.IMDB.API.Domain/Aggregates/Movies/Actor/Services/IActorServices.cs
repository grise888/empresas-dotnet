using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Services
{
    public interface IActorService : IDisposable
    {
        
		public Actor InsertActor(Actor actor);

		public Actor UpdateActor(Actor actor);

		public Actor DeleteActor(int actorId);

		public Actor GetActor(int actorId);

		public List<Actor> GetActors();

    }
}
