using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Services
{
    public class ActorService : Service, IActorService
    {

        private readonly IActorRepository _actorRepository;

        public ActorService(IMediator mediator,
                              IActorRepository actorRepository
                              ) : base(mediator)
        {
            _actorRepository = actorRepository;
        }
        
		public Actor InsertActor(Actor actor)
		{
			_actorRepository.Add(actor);
			_actorRepository.SaveChanges();
			return actor;
		}

		public Actor UpdateActor(Actor actor)
		{
			_actorRepository.Update(actor);
			_actorRepository.SaveChanges();
			return actor;
		}

		public Actor DeleteActor(int actorId)
		{
			Actor actor = _actorRepository.Remove(actorId);
			_actorRepository.SaveChanges();
			return actor;
		}

		public Actor GetActor(int actorId)
		{
			 return _actorRepository.Find(x => x.ActorId == actorId).FirstOrDefault();
		}

		public List<Actor> GetActors()
		{
			 return _actorRepository.GetAll().ToList();
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _actorRepository.Dispose();
        }

    }
}