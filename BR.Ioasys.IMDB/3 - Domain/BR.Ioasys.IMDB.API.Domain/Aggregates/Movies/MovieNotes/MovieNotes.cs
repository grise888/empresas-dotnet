namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes
{
	using System.Collections.Generic;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
	
	public class MovieNotes : EntityCore<MovieNotes>
	{  
	    
		public int MovieNoteId { get; set; }
		
		public int MovieId { get; set; }
		
		public Movie Movie { get; set; }
		
		public int UserId { get; set; }
		
		public User User { get; set; }
		
		public int Note { get; set; }
		
		#region Validations
		public bool IsValid()
		{
			return base.IsValid(this);
		}
		#endregion
	}
}