using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Repository;
using BR.Ioasys.IMDB.API.Domain.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Services
{
    public class MovieNotesService : Service, IMovieNotesService
    {

        private readonly IMovieNotesRepository _movieNotesRepository;

        public MovieNotesService(IMediator mediator,
                              IMovieNotesRepository movieNotesRepository
                              ) : base(mediator)
        {
            _movieNotesRepository = movieNotesRepository;
        }
        
		public void InsertMovieNotes(MovieNotes movieNotes)
		{
			_movieNotesRepository.Add(movieNotes);
			_movieNotesRepository.SaveChanges();
		}

		public void UpdateMovieNotes(MovieNotes movieNotes)
		{
			_movieNotesRepository.Update(movieNotes);
			_movieNotesRepository.SaveChanges();
		}

		public void DeleteMovieNotes(int movieNoteId)
		{
			_movieNotesRepository.Remove(movieNoteId);
			_movieNotesRepository.SaveChanges();
		}

		public MovieNotes GetMovieNotes(int movieNoteId)
		{
			 return _movieNotesRepository.Find(x => x.MovieNoteId == movieNoteId).FirstOrDefault();
		}

		public List<MovieNotes> GetMoviesNotes()
		{
			 return _movieNotesRepository.GetAll().ToList();
		}

		public List<MovieNotes> GetMovieNotesByMovie(int movId)
		{
			 return _movieNotesRepository.GetMovieNotesByMovie(movId);
		}

		public List<MovieNotes> GetMovieNotesByUser(int userId)
		{
			 return _movieNotesRepository.GetMovieNotesByUser(userId);
		}

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _movieNotesRepository.Dispose();
        }

    }
}