using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Services
{
    public interface IMovieNotesService : IDisposable
    {
        
		public void InsertMovieNotes(MovieNotes movieNotes);

		public void UpdateMovieNotes(MovieNotes movieNotes);

		public void DeleteMovieNotes(int movieNoteId);

		public MovieNotes GetMovieNotes(int movieNoteId);

		public List<MovieNotes> GetMoviesNotes();

		public List<MovieNotes> GetMovieNotesByMovie(int movieId);

		public List<MovieNotes> GetMovieNotesByUser(int userId);

    }
}
