using BR.Ioasys.IMDB.API.Domain.Interfaces;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Repository
{
	using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
    public interface IMovieNotesRepository : IRepository<MovieNotes>
    {
		
		public List<MovieNotes> GetMovieNotesByMovie(int movieId);

		public List<MovieNotes> GetMovieNotesByUser(int userId);

    }
}