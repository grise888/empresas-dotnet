using MediatR;
using Microsoft.Extensions.DependencyInjection;
using BR.Ioasys.IMDB.API.Services.Middlewares;
using BR.Ioasys.IMDB.Infra.CrossCutting.Ioc;

namespace BR.Ioasys.IMDB.API.Services.Configuration
{
    public static class DependencyInjectionConfiguration
    {
        public static void AddDIConfiguration(this IServiceCollection services)
        {
            BootStrapper.RegisterServices(services);           

            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(MediatorMiddleware<,>));
        }
    }
}
