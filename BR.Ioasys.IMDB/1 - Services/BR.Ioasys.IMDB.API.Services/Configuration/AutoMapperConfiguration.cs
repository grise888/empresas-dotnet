using AutoMapper;
using BR.Ioasys.IMDB.API.Application.AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BR.Ioasys.IMDB.API.Services.Configuration
{
    public static class AutoMapperConfiguration
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            AutoMapperConfig.RegisterMappings();
        }
    }
}
