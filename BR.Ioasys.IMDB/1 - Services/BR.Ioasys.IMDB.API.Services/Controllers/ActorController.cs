namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class ActorController : BaseController
    {
        private readonly IActorAppService _actorAppService;

        public ActorController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IActorAppService actorAppService) : base(notifications, mediator)
        {
            _actorAppService = actorAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertActor")]
		public IActionResult InsertActor([FromBody] ActorModel actor)
		{
			try
			{
				_actorAppService.InsertActor(actor);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateActor")]
		public IActionResult UpdateActor([FromBody] ActorModel actor)
		{
			try
			{
				_actorAppService.UpdateActor(actor);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteActor")]
		public IActionResult DeleteActor([FromBody] int actorId)
		{
			try
			{
				_actorAppService.DeleteActor(actorId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetActor")]
		public IActionResult GetActor(int actorId)
		{
			try
			{
				return Response(_actorAppService.GetActor(actorId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetActors")]
		public IActionResult GetActors()
		{
			try
			{
				return Response(_actorAppService.GetActors());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}