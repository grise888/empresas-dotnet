namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class UserController : BaseController
    {
        private readonly IUserAppService _userAppService;

        public UserController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IUserAppService userAppService) : base(notifications, mediator)
        {
            _userAppService = userAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertUser")]
		public IActionResult InsertUser([FromBody] UserModel user)
		{
			try
			{
				_userAppService.InsertUser(user);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateUser")]
		public IActionResult UpdateUser([FromBody] UserModel user)
		{
			try
			{
				_userAppService.UpdateUser(user);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteUser")]
		public IActionResult DeleteUser([FromBody] int userId)
		{
			try
			{
				_userAppService.InactivateUser(userId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("ActivateUser")]
		public IActionResult ActivateUser([FromBody] int userId)
		{
			try
			{
				_userAppService.ActivateUser(userId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUser")]
		public IActionResult GetUser(int userId)
		{
			try
			{
				return Response(_userAppService.GetUser(userId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUsers")]
		public IActionResult GetUsers()
		{
			try
			{
				return Response(_userAppService.GetUsers());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUserByProfile")]
		public IActionResult GetUserByUserProfile(int profileId)
		{
			try
			{
				return Response(_userAppService.GetUserByUserProfile(profileId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUserByStatus")]
		public IActionResult GetUserByUserStatus(int statusId)
		{
			try
			{
				return Response(_userAppService.GetUserByUserStatus(statusId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetActiveClients")]
		public IActionResult GetActiveClients(UserFilterModel filter)
		{
			try
			{
				return Response(_userAppService.GetActiveClients(filter));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUsersByFilter")]
		public IActionResult GetUsersByFilter(UserModel user)
		{
			try
			{
				return Response(_userAppService.GetUsersByFilter(user));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}
	}
}