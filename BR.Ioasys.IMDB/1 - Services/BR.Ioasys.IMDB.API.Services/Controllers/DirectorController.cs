namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class DirectorController : BaseController
    {
        private readonly IDirectorAppService _directorAppService;

        public DirectorController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IDirectorAppService directorAppService) : base(notifications, mediator)
        {
            _directorAppService = directorAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertDirector")]
		public IActionResult InsertDirector([FromBody] DirectorModel director)
		{
			try
			{
				_directorAppService.InsertDirector(director);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateDirector")]
		public IActionResult UpdateDirector([FromBody] DirectorModel director)
		{
			try
			{
				_directorAppService.UpdateDirector(director);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteDirector")]
		public IActionResult DeleteDirector([FromBody] int directorId)
		{
			try
			{
				_directorAppService.DeleteDirector(directorId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetDirector")]
		public IActionResult GetDirector(int directorId)
		{
			try
			{
				return Response(_directorAppService.GetDirector(directorId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetDirectors")]
		public IActionResult GetDirectors()
		{
			try
			{
				return Response(_directorAppService.GetDirectors());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}