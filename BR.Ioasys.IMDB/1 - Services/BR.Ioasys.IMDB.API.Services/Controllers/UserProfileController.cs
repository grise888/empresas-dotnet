namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class UserProfileController : BaseController
    {
        private readonly IUserProfileAppService _userProfileAppService;

        public UserProfileController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IUserProfileAppService userProfileAppService) : base(notifications, mediator)
        {
            _userProfileAppService = userProfileAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertUserProfile")]
		public IActionResult InsertUserProfile([FromBody] UserProfileModel userProfile)
		{
			try
			{
				_userProfileAppService.InsertUserProfile(userProfile);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateUserProfile")]
		public IActionResult UpdateUserProfile([FromBody] UserProfileModel userProfile)
		{
			try
			{
				_userProfileAppService.UpdateUserProfile(userProfile);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteUserProfile")]
		public IActionResult DeleteUserProfile([FromBody] int profileId)
		{
			try
			{
				_userProfileAppService.DeleteUserProfile(profileId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUserProfile")]
		public IActionResult GetUserProfile(int profileId)
		{
			try
			{
				return Response(_userProfileAppService.GetUserProfile(profileId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetProfiles")]
		public IActionResult GetProfiles()
		{
			try
			{
				return Response(_userProfileAppService.GetProfiles());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}