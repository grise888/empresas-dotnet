namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class MovieActorsController : BaseController
    {
        private readonly IMovieActorsAppService _movieActorsAppService;

        public MovieActorsController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IMovieActorsAppService movieActorsAppService) : base(notifications, mediator)
        {
            _movieActorsAppService = movieActorsAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertMovieActors")]
		public IActionResult InsertMovieActors([FromBody] MovieActorsModel movieActors)
		{
			try
			{
				_movieActorsAppService.InsertMovieActors(movieActors);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateMovieActors")]
		public IActionResult UpdateMovieActors([FromBody] MovieActorsModel movieActors)
		{
			try
			{
				_movieActorsAppService.UpdateMovieActors(movieActors);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteMovieActors")]
		public IActionResult DeleteMovieActors([FromBody] int movieActorId)
		{
			try
			{
				_movieActorsAppService.DeleteMovieActors(movieActorId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieActors")]
		public IActionResult GetMovieActors(int movieActorId)
		{
			try
			{
				return Response(_movieActorsAppService.GetMovieActors(movieActorId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMoviesActors")]
		public IActionResult GetMoviesActors()
		{
			try
			{
				return Response(_movieActorsAppService.GetMoviesActors());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieActorsByActor")]
		public IActionResult GetMovieActorsByActor(int actorId)
		{
			try
			{
				return Response(_movieActorsAppService.GetMovieActorsByActor(actorId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieActorsByMovie")]
		public IActionResult GetMovieActorsByMovie(int movieId)
		{
			try
			{
				return Response(_movieActorsAppService.GetMovieActorsByMovie(movieId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}