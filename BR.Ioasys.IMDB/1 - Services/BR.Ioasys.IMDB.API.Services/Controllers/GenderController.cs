namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class GenderController : BaseController
    {
        private readonly IGenderAppService _genderAppService;

        public GenderController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IGenderAppService genderAppService) : base(notifications, mediator)
        {
            _genderAppService = genderAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertGender")]
		public IActionResult InsertGender([FromBody] GenderModel gender)
		{
			try
			{
				_genderAppService.InsertGender(gender);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateGender")]
		public IActionResult UpdateGender([FromBody] GenderModel gender)
		{
			try
			{
				_genderAppService.UpdateGender(gender);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteGender")]
		public IActionResult DeleteGender([FromBody] int genderId)
		{
			try
			{
				_genderAppService.DeleteGender(genderId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetGender")]
		public IActionResult GetGender(int genderId)
		{
			try
			{
				return Response(_genderAppService.GetGender(genderId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetGenres")]
		public IActionResult GetGenres()
		{
			try
			{
				return Response(_genderAppService.GetGenres());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}