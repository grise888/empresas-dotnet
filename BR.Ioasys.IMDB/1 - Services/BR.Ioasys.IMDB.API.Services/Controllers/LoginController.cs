﻿using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Ioasys.IMDB.API.Services.Controllers
{
	[Route("api")]
	public class LoginController : BaseController
	{

		private readonly IUserAppService _userAppService;

		public LoginController(INotificationHandler<DomainNotification> notifications,
											   IMediator mediator,
											   IUserAppService userAppService) : base(notifications, mediator)
		{
			_userAppService = userAppService;
		}

		[AllowAnonymous]
		[HttpPost]
		[Route("Signin")]
		public IActionResult Signin([FromBody] LoginModel loginModel)
		{
			try
			{
				return Response(_userAppService.Signin(loginModel.Login, loginModel.Password));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

	}
}
