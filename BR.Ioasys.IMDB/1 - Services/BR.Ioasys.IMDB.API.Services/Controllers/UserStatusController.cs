namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class UserStatusController : BaseController
    {
        private readonly IUserStatusAppService _userStatusAppService;

        public UserStatusController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IUserStatusAppService userStatusAppService) : base(notifications, mediator)
        {
            _userStatusAppService = userStatusAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertUserStatus")]
		public IActionResult InsertUserStatus([FromBody] UserStatusModel userStatus)
		{
			try
			{
				_userStatusAppService.InsertUserStatus(userStatus);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateUserStatus")]
		public IActionResult UpdateUserStatus([FromBody] UserStatusModel userStatus)
		{
			try
			{
				_userStatusAppService.UpdateUserStatus(userStatus);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteUserStatus")]
		public IActionResult DeleteUserStatus([FromBody] int statusId)
		{
			try
			{
				_userStatusAppService.DeleteUserStatus(statusId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUserStatus")]
		public IActionResult GetUserStatus(int statusId)
		{
			try
			{
				return Response(_userStatusAppService.GetUserStatus(statusId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetUsersStatus")]
		public IActionResult GetUsersStatus()
		{
			try
			{
				return Response(_userStatusAppService.GetUsersStatus());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}