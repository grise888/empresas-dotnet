namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class MovieNotesController : BaseController
    {
        private readonly IMovieNotesAppService _movieNotesAppService;

        public MovieNotesController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IMovieNotesAppService movieNotesAppService) : base(notifications, mediator)
        {
            _movieNotesAppService = movieNotesAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertMovieNotes")]
		public IActionResult InsertMovieNotes([FromBody] MovieNotesModel movieNotes)
		{
			try
			{
				_movieNotesAppService.InsertMovieNotes(movieNotes);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateMovieNotes")]
		public IActionResult UpdateMovieNotes([FromBody] MovieNotesModel movieNotes)
		{
			try
			{
				_movieNotesAppService.UpdateMovieNotes(movieNotes);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteMovieNotes")]
		public IActionResult DeleteMovieNotes([FromBody] int movieNoteId)
		{
			try
			{
				_movieNotesAppService.DeleteMovieNotes(movieNoteId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieNotes")]
		public IActionResult GetMovieNotes(int movieNoteId)
		{
			try
			{
				return Response(_movieNotesAppService.GetMovieNotes(movieNoteId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Route("GetMoviesNotes")]
		public IActionResult GetMoviesNotes()
		{
			try
			{
				return Response(_movieNotesAppService.GetMoviesNotes());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieNotesByMovie")]
		public IActionResult GetMovieNotesByMovie(int movieId)
		{
			try
			{
				return Response(_movieNotesAppService.GetMovieNotesByMovie(movieId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieNotesByUser")]
		public IActionResult GetMovieNotesByUser(int userId)
		{
			try
			{
				return Response(_movieNotesAppService.GetMovieNotesByUser(userId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

    }
}