namespace BR.Ioasys.IMDB.API.Services.Controllers
{
    using BR.Ioasys.IMDB.API.Application.Interfaces;
    using BR.Ioasys.IMDB.API.Application.ViewModels;
    using BR.Ioasys.IMDB.API.Domain.Notifications;
    using BR.Ioasys.IMDB.API.Services.Controllers.Core;
    using BR.Ioasys.IMDB.API.Domain.Exceptions;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claase para controller da API
    /// </summary>
    [Route("api")]
    public class MovieController : BaseController
    {
        private readonly IMovieAppService _movieAppService;

        public MovieController(INotificationHandler<DomainNotification> notifications,
                                               IMediator mediator,
                                               IMovieAppService movieAppService) : base(notifications, mediator)
        {
            _movieAppService = movieAppService;
        }
        
		[HttpPost]
		[Authorize("Bearer")]
		[Route("InsertMovie")]
		public IActionResult InsertMovie([FromBody] MovieModel movie)
		{
			try
			{
				_movieAppService.InsertMovie(movie);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("UpdateMovie")]
		public IActionResult UpdateMovie([FromBody] MovieModel movie)
		{
			try
			{
				_movieAppService.UpdateMovie(movie);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpPost]
		[Authorize("Bearer")]
		[Route("DeleteMovie")]
		public IActionResult DeleteMovie([FromBody] int movieId)
		{
			try
			{
				_movieAppService.DeleteMovie(movieId);
				return Response();
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovie")]
		public IActionResult GetMovie(int movieId)
		{
			try
			{
				return Response(_movieAppService.GetMovie(movieId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovies")]
		public IActionResult GetMovies()
		{
			try
			{
				return Response(_movieAppService.GetMovies());
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieByGender")]
		public IActionResult GetMovieByGender(int genId)
		{
			try
			{
				return Response(_movieAppService.GetMovieByGender(genId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMovieByDirector")]
		public IActionResult GetMovieByDirector(int directorId)
		{
			try
			{
				return Response(_movieAppService.GetMovieByDirector(directorId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("GetMoviesByFilter")]
		public IActionResult GetMoviesByFilter(MovieFilterModel movie)
		{
			try
			{
				return Response(_movieAppService.GetMoviesByFilter(movie));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

		[HttpGet]
		[Authorize("Bearer")]
		[Route("DetailMovie")]
		public IActionResult DetailMovie(int movieId)
		{
			try
			{
				return Response(_movieAppService.DetailMovie(movieId));
			}
			catch (Exception ex)
			{
				RaiseError(ex.Message);
				return Response(null, ex.Message, null);
			}
		}

	}
}