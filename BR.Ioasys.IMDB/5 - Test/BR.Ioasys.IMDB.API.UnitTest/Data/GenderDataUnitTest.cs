﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;


namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class GenderDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IGenderRepository _genderRepository;

        public GenderDataUnitTest(CoreDataTest CoreDataTest, IGenderRepository genderRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _genderRepository = genderRepository;
        }

        [Fact(DisplayName = "Insert Gender")]
        [Trait("Insert", "Gender")]
        public void TestInsertGender()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Gender gender = new Gender
                {
                    Name = "Ficção"
                };

                var response = _genderRepository.Add(gender);
                Assert.NotNull(response);
                Assert.Equal(gender.Name, response.Name);
                Assert.False(response.GenderId == 0);
            }
        }


        [Fact(DisplayName = "Update Gender")]
        [Trait("Update", "Gender")]
        public void TestUpdateGender()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Gender gender = _genderRepository.GetById(1);
                gender.Name = "Acao";
                var response = _genderRepository.Update(gender);
                Assert.NotNull(response);
                Assert.Equal(gender.Name, response.Name);
            }
        }


        [Fact(DisplayName = "Delete Gender")]
        [Trait("Delete", "Gender")]
        public void TestDeleteGender()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Gender gender = _genderRepository.GetAll().OrderByDescending(x => x.GenderId).FirstOrDefault();
                var response = _genderRepository.Remove(gender);
                Assert.NotNull(response);
                Assert.Equal(gender.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Gender")]
        [Trait("Get", "Gender")]
        public void TestGetGender()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int genderId = 1;
                Gender gender = _genderRepository.GetById(genderId);
                var response = _genderRepository.GetById(genderId);
                Assert.NotNull(response);
                Assert.Equal(gender.GenderId, response.GenderId);
                Assert.Equal(gender.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Genes")]
        [Trait("Get", "Gender")]
        public void TestGetGeners()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _genderRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

    }
}
