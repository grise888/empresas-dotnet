﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class UserDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IUserRepository _userRepository;

        public UserDataUnitTest(CoreDataTest CoreDataTest, IUserRepository userRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _userRepository = userRepository;
        }

        [Fact(DisplayName = "Insert User")]
        [Trait("Insert", "User")]
        public void TestInsertUser()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                User user = new User
                {
                    Name = "Geraldo Grise bacelar de sousa",
                    Email = "grise888@gmail.com",
                    Login = "geraldo.grise",
                    Password = "Pass@123",
                    ProfileId = 1,
                    StatusId = 1
                };

                var response = _userRepository.Add(user);
                Assert.NotNull(response);
                Assert.Equal(user.Name, response.Name);
                Assert.Equal(user.Email, response.Email);
                Assert.Equal(user.Login, response.Login);
                Assert.Equal(user.Password, response.Password);
                Assert.Equal(user.ProfileId, response.ProfileId);
                Assert.Equal(user.StatusId, response.StatusId);
                Assert.False(response.UserId == 0);
            }
        }


        [Fact(DisplayName = "Update User")]
        [Trait("Update", "User")]
        public void TestUpdateUser()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                User user = _userRepository.GetById(1);
                user.Name = "Geraldo grise";
                var response = _userRepository.Update(user);
                Assert.NotNull(response);
                Assert.Equal(user.Name, response.Name);
                Assert.Equal(user.Email, response.Email);
                Assert.Equal(user.Login, response.Login);
                Assert.Equal(user.Password, response.Password);
                Assert.Equal(user.ProfileId, response.ProfileId);
                Assert.Equal(user.StatusId, response.StatusId);
            }
        }


        [Fact(DisplayName = "Delete User")]
        [Trait("Delete", "User")]
        public void TestDeleteUser()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                User user = _userRepository.GetAll().OrderByDescending(x => x.UserId).FirstOrDefault();
                var response = _userRepository.Remove(user);
                Assert.NotNull(response);
                Assert.Equal(user.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get User")]
        [Trait("Get", "User")]
        public void TestGetUser()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int userId = 1;
                var user = _userRepository.GetById(userId);
                var response = _userRepository.GetById(userId);
                Assert.NotNull(response);
                Assert.Equal(user.UserId, response.UserId);
                Assert.Equal(user.Email, response.Email);
                Assert.Equal(user.Login, response.Login);
                Assert.Equal(user.Password, response.Password);
                Assert.Equal(user.ProfileId, response.ProfileId);
                Assert.Equal(user.StatusId, response.StatusId);
            }
        }

        [Fact(DisplayName = "Get User")]
        [Trait("Get", "User")]
        public void TestGetUsers()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _userRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }


        [Fact(DisplayName = "Get users by profile")]
        public void TestGetUsersByProfile()
        {
            int profileId = 1;
            List<User> users = _userRepository.GetUserByUserProfile(profileId);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

        [Fact(DisplayName = "Get users by status?")]
        public void GetUsersByStatus()
        {
            int statusId = 1;
            List<User> users = _userRepository.GetUserByUserProfile(statusId);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

        [Fact(DisplayName = "Get active clients?")]
        public void GetActiveClients()
        {
            UserFilter filter = new UserFilter()
            {
                NumberRecords = 10,
                Page = 1,
                Pagination = true
            };
            List<User> users = _userRepository.GetActiveClients(filter);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }


        [Fact(DisplayName = "is it possible to get active clients?")]
        public void GetUsersByFilter()
        {
            User filter = new User()
            {
                Name = "Geraldo",
            };
            List<User> users = _userRepository.GetUsersByFilter(filter);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

    }
}
