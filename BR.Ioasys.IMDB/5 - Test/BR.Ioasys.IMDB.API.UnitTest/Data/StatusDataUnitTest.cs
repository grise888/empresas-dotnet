﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;


namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class StatusDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IUserStatusRepository _userStatusRepository;

        public StatusDataUnitTest(CoreDataTest CoreDataTest, IUserStatusRepository userStatusRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _userStatusRepository = userStatusRepository;
        }

        [Fact(DisplayName = "Insert UserStatus")]
        [Trait("Insert", "UserStatus")]
        public void TestInsertUserStatus()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserStatus userStatus = new UserStatus
                {
                    Name = "Suspenso"
                };

                var response = _userStatusRepository.Add(userStatus);
                Assert.NotNull(response);
                Assert.Equal(userStatus.Name, response.Name);
                Assert.False(response.StatusId == 0);
            }
        }


        [Fact(DisplayName = "Update UserStatus")]
        [Trait("Update", "UserStatus")]
        public void TestUpdateUserStatus()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserStatus userStatus = _userStatusRepository.GetById(1);
                userStatus.Name = "Ativo";
                var response = _userStatusRepository.Update(userStatus);
                Assert.NotNull(response);
                Assert.Equal(userStatus.Name, response.Name);
            }
        }


        [Fact(DisplayName = "Delete UserStatus")]
        [Trait("Delete", "UserStatus")]
        public void TestDeleteUserStatus()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserStatus userStatus = _userStatusRepository.GetAll().OrderByDescending(x => x.StatusId).FirstOrDefault();
                var response = _userStatusRepository.Remove(userStatus);
                Assert.NotNull(response);
                Assert.Equal(userStatus.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get UserStatus")]
        [Trait("Get", "UserStatus")]
        public void TestGetUserStatus()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int userStatusId = 1;
                UserStatus status = _userStatusRepository.GetById(1);
                var response = _userStatusRepository.GetById(userStatusId);
                Assert.NotNull(response);
                Assert.Equal(status.StatusId, response.StatusId);
                Assert.Equal(status.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get UserStatus")]
        [Trait("Get", "UserStatus")]
        public void TestGetUserStatuss()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _userStatusRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

    }
}
