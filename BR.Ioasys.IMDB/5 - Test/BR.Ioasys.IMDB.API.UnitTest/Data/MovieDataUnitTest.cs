﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;
using System.Collections.Generic;

namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class MovieDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IMovieRepository _movieRepository;

        public MovieDataUnitTest(CoreDataTest CoreDataTest, IMovieRepository movieRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _movieRepository = movieRepository;
        }

        [Fact(DisplayName = "Insert Movie")]
        [Trait("Insert", "Movie")]
        public void TestInsertMovie()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Movie movie = new Movie
                {
                    Name = "VIÚVA NEGRA",
                    DirectorId = 1,
                    GenderId = 2,
                    Detail = "No novo filme da Marvel Studios, \"Viúva Negra\", Natasha Romanoff (Scarlett Johansson) precisa confrontar partes de sua história quando surge uma conspiração perigosa ligada ao seu passado. Perseguida por uma força que não irá parar até derrotá-la, Natasha terá que lidar com sua antiga vida de espiã, e também reencontrar membros de sua família que deixou para trás antes de se tornar parte dos Vingadores."
                };

                var response = _movieRepository.Add(movie);
                Assert.NotNull(response);
                Assert.Equal(movie.Name, response.Name);
                Assert.Equal(movie.Detail, response.Detail);
                Assert.Equal(movie.DirectorId, response.DirectorId);
                Assert.Equal(movie.GenderId, response.GenderId);
                Assert.False(response.MovieId == 0);
            }
        }


        [Fact(DisplayName = "Update Movie")]
        [Trait("Update", "Movie")]
        public void TestUpdateMovie()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Movie movie = _movieRepository.GetById(1);
                movie.Name = "GODZILLA VS KONG";
                var response = _movieRepository.Update(movie);
                Assert.NotNull(response);
                Assert.Equal(movie.Name, response.Name);
                Assert.Equal(movie.Detail, response.Detail);
                Assert.Equal(movie.DirectorId, response.DirectorId);
                Assert.Equal(movie.GenderId, response.GenderId);
            }
        }


        [Fact(DisplayName = "Delete Movie")]
        [Trait("Delete", "Movie")]
        public void TestDeleteMovie()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Movie movie = _movieRepository.GetAll().OrderByDescending(x => x.MovieId).FirstOrDefault();
                var response = _movieRepository.Remove(movie);
                Assert.NotNull(response);
                Assert.Equal(movie.Name, response.Name);
                Assert.Equal(movie.Detail, response.Detail);
                Assert.Equal(movie.DirectorId, response.DirectorId);
                Assert.Equal(movie.GenderId, response.GenderId);
            }
        }

        [Fact(DisplayName = "Get Movie")]
        [Trait("Get", "Movie")]
        public void TestGetMovie()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int movieId = 1;
                Movie movie = _movieRepository.GetById(movieId);
                var response = _movieRepository.GetById(movieId);
                Assert.NotNull(response);
                Assert.Equal(movie.MovieId, response.MovieId);
                Assert.Equal(movie.Name, response.Name);
                Assert.Equal(movie.Detail, response.Detail);
                Assert.Equal(movie.DirectorId, response.DirectorId);
                Assert.Equal(movie.GenderId, response.GenderId);
            }
        }

        [Fact(DisplayName = "Get Movies")]
        [Trait("GetAll", "Movie")]
        public void TestGetMovies()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _movieRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

        [Fact(DisplayName = "Get movies by director?")]
        public void GetMovieByDirector()
        {
            int directorId = 1;
            List<Movie> movies = _movieRepository.GetMovieByDirector(directorId);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }

        [Fact(DisplayName = "Get movies by gender?")]
        public void GetMovieByGender()
        {
            int genderId = 1;
            List<Movie> movies = _movieRepository.GetMovieByGender(genderId);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }


        [Fact(DisplayName = "Get movies by filter?")]
        public void TestMoviesByFilter()
        {
            Movie filter = new Movie()
            {
                Name = "Mulher",
                GenderName = "Ação"
            };
            List<Movie> movies = _movieRepository.GetMoviesByFilter(filter);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }

    }
}
