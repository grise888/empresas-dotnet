﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;


namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class ProfileDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IUserProfileRepository _userProfileRepository;

        public ProfileDataUnitTest(CoreDataTest CoreDataTest, IUserProfileRepository userProfileRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _userProfileRepository = userProfileRepository;
        }

        [Fact(DisplayName = "Insert Profile")]
        [Trait("Insert", "UserProfile")]
        public void TestInsertUserProfile()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserProfile userProfile = new UserProfile
                {
                    Name = "Editor"
                };

                var response = _userProfileRepository.Add(userProfile);
                Assert.NotNull(response);
                Assert.Equal(userProfile.Name, response.Name);
                Assert.False(response.ProfileId == 0);
            }
        }


        [Fact(DisplayName = "Update Profile")]
        [Trait("Update", "UserProfile")]
        public void TestUpdateUserProfile()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserProfile userProfile = _userProfileRepository.GetById(1);
                userProfile.Name = "Admin";
                var response = _userProfileRepository.Update(userProfile);
                Assert.NotNull(response);
                Assert.Equal(userProfile.Name, response.Name);
            }
        }


        [Fact(DisplayName = "Delete Profile")]
        [Trait("Delete", "UserProfile")]
        public void TestDeleteUserProfile()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                UserProfile userProfile = _userProfileRepository.GetAll().OrderByDescending(x => x.ProfileId).FirstOrDefault();
                var response = _userProfileRepository.Remove(userProfile);
                Assert.NotNull(response);
                Assert.Equal(userProfile.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Profile")]
        [Trait("Get", "UserProfile")]
        public void TestGetUserProfile()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int userProfileId = 1;
                UserProfile profile = _userProfileRepository.GetById(userProfileId);
                var response = _userProfileRepository.GetById(userProfileId);
                Assert.NotNull(response);
                Assert.Equal(profile.ProfileId, response.ProfileId);
                Assert.Equal(profile.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get rofiles")]
        [Trait("Get", "UserProfile")]
        public void TestGetUserProfiles()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _userProfileRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

    }
}
