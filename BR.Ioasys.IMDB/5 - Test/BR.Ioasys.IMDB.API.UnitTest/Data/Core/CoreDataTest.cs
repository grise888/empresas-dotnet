﻿using BR.Ioasys.IMDB.Infra.Data.Context;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace BR.Ioasys.IMDB.API.UnitTest.Data.Core
{
    public class CoreDataTest
    {

        public ServiceProvider ServiceProvider { get; private set; }

        public CoreDataTest()
        {
            GetServiceCollection();
        }

        private ServiceCollection GetServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDbContext<DatabaseContext>(o =>
                o.UseSqlServer(GetConnectionString()),
                  ServiceLifetime.Transient
            );

            return serviceCollection;
        }

        private void GetServiceProvider()
        {
            var serviceCollection = GetServiceCollection();
            ServiceProvider = serviceCollection.BuildServiceProvider();
            using (var context = ServiceProvider.GetService<DatabaseContext>())
            {
                context.Database.EnsureCreated();
            }
        }



        private string GetConnectionString()
        {
            var config = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json")
                 .Build();
            return config.GetConnectionString("DefaultConnection");
        }

        public void Dispose()
        {
            using (var context = ServiceProvider.GetService<DatabaseContext>())
            {
                context.Database.EnsureDeleted();
            }
        }
    }
}
