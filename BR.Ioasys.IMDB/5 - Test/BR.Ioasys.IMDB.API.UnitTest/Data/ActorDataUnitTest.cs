﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;


namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class ActorDataUnitTest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IActorRepository _actorRepository;

        public ActorDataUnitTest(CoreDataTest CoreDataTest, IActorRepository actorRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _actorRepository = actorRepository;
        }

        [Fact(DisplayName = "Insert Actor")]
        [Trait("Insert", "Actor")]
        public void TestInsertActor()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Actor actor = new Actor
                {
                    Name = "Geraldo Grise"
                };

                var response = _actorRepository.Add(actor);
                Assert.NotNull(response);
                Assert.Equal(actor.Name, response.Name);
                Assert.False(response.ActorId == 0);
            }
        }


        [Fact(DisplayName = "Update Actor")]
        [Trait("Update", "Actor")]
        public void TestUpdateActor()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Actor actor = _actorRepository.GetById(1);
                actor.Name = "Kyle Chandler";
                var response = _actorRepository.Update(actor);
                Assert.NotNull(response);
                Assert.Equal(actor.ActorId, response.ActorId);
                Assert.Equal(actor.Name, response.Name);
            }
        }


        [Fact(DisplayName = "Delete Actor")]
        [Trait("Delete", "Actor")]
        public void TestDeleteActor()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Actor actor = _actorRepository.GetAll().OrderByDescending(x => x.ActorId).FirstOrDefault();
                var response = _actorRepository.Remove(actor);
                Assert.NotNull(response);
                Assert.Equal(actor.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Actor")]
        [Trait("Get", "Actor")]
        public void TestGetActor()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int actorId = 1;
                Actor actor = _actorRepository.GetById(actorId);
                var response = _actorRepository.GetById(actorId);
                Assert.NotNull(response);
                Assert.Equal(actor.ActorId, response.ActorId);
                Assert.Equal(actor.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Actors")]
        [Trait("Get", "Actor")]
        public void TestGetActors()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _actorRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

    }
}
