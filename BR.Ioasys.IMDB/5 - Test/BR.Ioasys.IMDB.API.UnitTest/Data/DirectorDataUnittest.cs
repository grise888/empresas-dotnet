﻿using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Repository;
using BR.Ioasys.IMDB.API.UnitTest.Data.Core;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using System.Linq;


namespace BR.Ioasys.IMDB.API.UnitTest.Data
{
    public class DirectorDataUnittest : CoreDataTest, IClassFixture<CoreDataTest>
    {
        private ServiceProvider _serviceProvide;
        private readonly IDirectorRepository _directorRepository;

        public DirectorDataUnittest(CoreDataTest CoreDataTest, IDirectorRepository directorRepository)
        {
            _serviceProvide = CoreDataTest.ServiceProvider;
            _directorRepository = directorRepository;
        }

        [Fact(DisplayName = "Insert Director")]
        [Trait("Insert", "Director")]
        public void TestInsertDirector()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Director director = new Director
                {
                    Name = "Ficção"
                };

                var response = _directorRepository.Add(director);
                Assert.NotNull(response);
                Assert.Equal(director.Name, response.Name);
                Assert.False(response.DirectorId == 0);
            }
        }


        [Fact(DisplayName = "Update Director")]
        [Trait("Update", "Director")]
        public void TestUpdateDirector()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Director director = _directorRepository.GetById(1);
                director.Name = "Geraldo Grise";
                var response = _directorRepository.Update(director);
                Assert.NotNull(response);
                Assert.Equal(director.Name, response.Name);
            }
        }


        [Fact(DisplayName = "Delete Director")]
        [Trait("Delete", "Director")]
        public void TestDeleteDirector()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                Director director = _directorRepository.GetAll().OrderByDescending(x => x.DirectorId).FirstOrDefault();
                var response = _directorRepository.Remove(director);
                Assert.NotNull(response);
                Assert.Equal(director.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Director")]
        [Trait("Get", "Director")]
        public void TestGetDirector()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                int directorId = 1;
                Director director = _directorRepository.GetById(directorId);
                var response = _directorRepository.GetById(directorId);
                Assert.NotNull(response);
                Assert.Equal(director.DirectorId, response.DirectorId);
                Assert.Equal(director.Name, response.Name);
            }
        }

        [Fact(DisplayName = "Get Directors")]
        [Trait("Get", "Director")]
        public void TestGetDirectors()
        {
            using (var context = _serviceProvide.GetService<DatabaseContext>())
            {
                var response = _directorRepository.GetAll();
                Assert.NotNull(response);
                Assert.True(response.Count() > 0);
            }
        }

    }
}
