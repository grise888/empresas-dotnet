﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class DirectorAppUnitTest
    {
        private IDirectorAppService _directorAppService;
        private readonly IMapper _mapper;

        public DirectorAppUnitTest(IDirectorAppService directorAppService, IMapper mapper)

        {
            _directorAppService = directorAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert an director?")]
        public void InsertDirector()
        {
            DirectorModel director = new DirectorModel()
            {
                Name = Faker.Name.FullName(),
            };
            DirectorModel directorModel = _directorAppService.InsertDirector(director);
            Assert.NotNull(directorModel);
            Assert.Equal(directorModel.Name, director.Name);
        }


        [Fact(DisplayName = "is it possible to update an director?")]
        public void UpdateDirector()
        {

            int directorId = 1;
            DirectorModel director = _directorAppService.GetDirector(directorId);
            director.Name = "Adam Wingard";
            DirectorModel directorModel = _directorAppService.UpdateDirector(director);
            Assert.NotNull(directorModel);
            Assert.Equal(directorModel.Name, director.Name);
        }


        [Fact(DisplayName = "is it possible to delete an director?")]
        public void DeleteDirector()
        {

            int directorId = 3;
            DirectorModel directorModel = _directorAppService.DeleteDirector(directorId);
            Assert.NotNull(directorModel);
        }

        [Fact(DisplayName = "is it possible to get an director?")]
        public void GetDirector()
        {

            int directorId = 1;
            DirectorModel directorModel = _directorAppService.GetDirector(directorId);
            Assert.NotNull(directorModel);
        }

        [Fact(DisplayName = "is it possible to get all directors?")]
        public void GetDirectors()
        {
            List<DirectorModel> directors = _directorAppService.GetDirectors();
            Assert.NotNull(directors);
            Assert.True(directors.Count > 0);
        }
    }
}
