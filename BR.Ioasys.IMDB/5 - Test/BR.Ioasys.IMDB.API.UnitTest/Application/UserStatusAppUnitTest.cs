﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class UserStatusAppUnitTest
    {
        private IUserStatusAppService _userStatusAppService;
        private readonly IMapper _mapper;

        public UserStatusAppUnitTest(IUserStatusAppService userStatusAppService, IMapper mapper)

        {
            _userStatusAppService = userStatusAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert status?")]
        public void InsertUserStatus()
        {
            UserStatusModel userStatus = new UserStatusModel()
            {
                Name = Faker.Name.FullName(),
            };
            UserStatusModel userStatusModel = _userStatusAppService.InsertUserStatus(userStatus);
            Assert.NotNull(userStatusModel);
            Assert.Equal(userStatusModel.Name, userStatus.Name);
        }


        [Fact(DisplayName = "is it possible to update status?")]
        public void UpdateUserStatus()
        {

            int userStatusId = 1;
            UserStatusModel userStatus = _userStatusAppService.GetUserStatus(userStatusId);
            userStatus.Name = "Kyle Chandler";
            UserStatusModel userStatusModel = _userStatusAppService.UpdateUserStatus(userStatus);
            Assert.NotNull(userStatusModel);
            Assert.Equal(userStatusModel.Name, userStatus.Name);
        }


        [Fact(DisplayName = "is it possible to delete status?")]
        public void DeleteUserStatus()
        {

            int userStatusId = 7;
            UserStatusModel userStatusModel = _userStatusAppService.DeleteUserStatus(userStatusId);
            Assert.NotNull(userStatusModel);
        }

        [Fact(DisplayName = "is it possible to get status?")]
        public void GetUserStatus()
        {

            int userStatusId = 1;
            UserStatusModel userStatusModel = _userStatusAppService.GetUserStatus(userStatusId);
            Assert.NotNull(userStatusModel);
        }

        [Fact(DisplayName = "is it possible to get all status?")]
        public void GetUserStatuss()
        {
            List<UserStatusModel> userStatuss = _userStatusAppService.GetUsersStatus();
            Assert.NotNull(userStatuss);
            Assert.True(userStatuss.Count > 0);
        }
    }
}
