﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class ActorAppUnitTest
    {
        private IActorAppService _actorAppService;
        private readonly IMapper _mapper;

        public ActorAppUnitTest(IActorAppService actorAppService, IMapper mapper)

        {
            _actorAppService = actorAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert an actor?")]
        public void InsertActor()
        {
            ActorModel actor = new ActorModel()
            {
                Name = Faker.Name.FullName(),
            };
            ActorModel actorModel = _actorAppService.InsertActor(actor);
            Assert.NotNull(actorModel);
            Assert.Equal(actorModel.Name, actor.Name);
        }


        [Fact(DisplayName = "is it possible to update an actor?")]
        public void UpdateActor()
        {
          
            int actorId = 1;
            ActorModel actor = _actorAppService.GetActor(actorId);
            actor.Name = "Kyle Chandler";
            ActorModel actorModel = _actorAppService.UpdateActor(actor);
            Assert.NotNull(actorModel);
            Assert.Equal(actorModel.Name, actor.Name);
        }


        [Fact(DisplayName = "is it possible to delete an actor?")]
        public void DeleteActor()
        {

            int actorId = 7;
            ActorModel actorModel =  _actorAppService.DeleteActor(actorId);
            Assert.NotNull(actorModel);
        }

        [Fact(DisplayName = "is it possible to get an actor?")]
        public void GetActor()
        {

            int actorId = 1;
            ActorModel actorModel = _actorAppService.GetActor(actorId);
            Assert.NotNull(actorModel);
        }

        [Fact(DisplayName = "is it possible to get all actors?")]
        public void GetActors()
        {
           List<ActorModel> actors = _actorAppService.GetActors();
            Assert.NotNull(actors);
            Assert.True(actors.Count > 0);
        }
    }
}
