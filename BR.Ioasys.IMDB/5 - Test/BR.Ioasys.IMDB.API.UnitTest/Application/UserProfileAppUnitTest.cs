﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class UserProfileAppUnitTest
    {
        private IUserProfileAppService _userProfileAppService;
        private readonly IMapper _mapper;

        public UserProfileAppUnitTest(IUserProfileAppService userProfileAppService, IMapper mapper)

        {
            _userProfileAppService = userProfileAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert profile?")]
        public void InsertUserProfile()
        {
            UserProfileModel userProfile = new UserProfileModel()
            {
                Name = Faker.Name.FullName(),
            };
            UserProfileModel userProfileModel = _userProfileAppService.InsertUserProfile(userProfile);
            Assert.NotNull(userProfileModel);
            Assert.Equal(userProfileModel.Name, userProfile.Name);
        }


        [Fact(DisplayName = "is it possible to update profile?")]
        public void UpdateUserProfile()
        {

            int userProfileId = 1;
            UserProfileModel userProfile = _userProfileAppService.GetUserProfile(userProfileId);
            userProfile.Name = "Administrador";
            UserProfileModel userProfileModel = _userProfileAppService.UpdateUserProfile(userProfile);
            Assert.NotNull(userProfileModel);
            Assert.Equal(userProfileModel.Name, userProfile.Name);
        }


        [Fact(DisplayName = "is it possible to delete profile?")]
        public void DeleteUserProfile()
        {

            int userProfileId = 7;
            UserProfileModel userProfileModel = _userProfileAppService.DeleteUserProfile(userProfileId);
            Assert.NotNull(userProfileModel);
        }

        [Fact(DisplayName = "is it possible to get profile?")]
        public void GetUserProfile()
        {

            int userProfileId = 1;
            UserProfileModel userProfileModel = _userProfileAppService.GetUserProfile(userProfileId);
            Assert.NotNull(userProfileModel);
        }

        [Fact(DisplayName = "is it possible to get all profiles?")]
        public void GetProfiles()
        {
            List<UserProfileModel> userProfiles = _userProfileAppService.GetProfiles();
            Assert.NotNull(userProfiles);
            Assert.True(userProfiles.Count > 0);
        }
    }
}
