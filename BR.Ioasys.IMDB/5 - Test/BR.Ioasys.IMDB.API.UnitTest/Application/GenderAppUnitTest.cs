﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class GenderAppUnitTest
    {
        private IGenderAppService _genderAppService;
        private readonly IMapper _mapper;

        public GenderAppUnitTest(IGenderAppService genderAppService, IMapper mapper)

        {
            _genderAppService = genderAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert an gender?")]
        public void InsertGender()
        {
            GenderModel gender = new GenderModel()
            {
                Name = Faker.Name.FullName(),
            };
            GenderModel genderModel = _genderAppService.InsertGender(gender);
            Assert.NotNull(genderModel);
            Assert.Equal(genderModel.Name, gender.Name);
        }


        [Fact(DisplayName = "is it possible to update an gender?")]
        public void UpdateGender()
        {

            int genderId = 1;
            GenderModel gender = _genderAppService.GetGender(genderId);
            gender.Name = "Ação";
            GenderModel genderModel = _genderAppService.UpdateGender(gender);
            Assert.NotNull(genderModel);
            Assert.Equal(genderModel.Name, gender.Name);
        }


        [Fact(DisplayName = "is it possible to delete an gender?")]
        public void DeleteGender()
        {

            int genderId = 7;
            GenderModel genderModel = _genderAppService.DeleteGender(genderId);
            Assert.NotNull(genderModel);
        }

        [Fact(DisplayName = "is it possible to get an gender?")]
        public void GetGender()
        {

            int genderId = 1;
            GenderModel genderModel = _genderAppService.GetGender(genderId);
            Assert.NotNull(genderModel);
        }

        [Fact(DisplayName = "is it possible to get all geners?")]
        public void GetGenres()
        {
            List<GenderModel> genders = _genderAppService.GetGenres();
            Assert.NotNull(genders);
            Assert.True(genders.Count > 0);
        }
    }
}
