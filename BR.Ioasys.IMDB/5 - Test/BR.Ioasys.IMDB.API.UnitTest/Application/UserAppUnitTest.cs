﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class UserAppUnitTest
    {
        private IUserAppService _userAppService;
        private readonly IMapper _mapper;

        public UserAppUnitTest(IUserAppService userAppService, IMapper mapper)

        {
            _userAppService = userAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert a user?")]
        public void InsertUser()
        {
            UserModel user = new UserModel()
            {
                Name = Faker.Name.FullName(),
            };
            UserModel userModel = _userAppService.InsertUser(user);
            Assert.NotNull(userModel);
            Assert.Equal(userModel.Name, user.Name);
        }


        [Fact(DisplayName = "is it possible to update a user?")]
        public void UpdateUser()
        {

            int userId = 1;
            UserModel user = _userAppService.GetUser(userId);
            user.Name = "Geraldo Grise";
            UserModel userModel = _userAppService.UpdateUser(user);
            Assert.NotNull(userModel);
            Assert.Equal(userModel.Name, user.Name);
        }


        [Fact(DisplayName = "is it possible to delete a user?")]
        public void DeleteUser()
        {

            int userId = 7;
            UserModel userModel = _userAppService.DeleteUser(userId);
            Assert.NotNull(userModel);
        }

        [Fact(DisplayName = "is it possible to get a user?")]
        public void GetUser()
        {

            int userId = 1;
            UserModel userModel = _userAppService.GetUser(userId);
            Assert.NotNull(userModel);
        }

        [Fact(DisplayName = "is it possible to get all users?")]
        public void GetUsers()
        {
            List<UserModel> users = _userAppService.GetUsers();
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

        [Fact(DisplayName = "is it possible to get users by profile?")]
        public void GetUsersByProfile()
        {
            int profileId = 1;
            List<UserModel> users = _userAppService.GetUserByUserProfile(profileId);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

        [Fact(DisplayName = "is it possible to get users by status?")]
        public void GetUsersByStatus()
        {
            int statusId = 1;
            List<UserModel> users = _userAppService.GetUserByUserProfile(statusId);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }

        [Fact(DisplayName = "is it possible to get active clients?")]
        public void GetActiveClients()
        {
            UserFilterModel filter = new UserFilterModel()
            {
                NumberRecords = 10,
                Page = 1,
                Pagination = true
            };
            List<UserModel> users = _userAppService.GetActiveClients(filter);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }


        [Fact(DisplayName = "is it possible to get users by filter?")]
        public void GetUsersByFilter()
        {
            UserModel filter = new UserModel()
            {
               Name = "Geraldo",
            };
            List<UserModel> users = _userAppService.GetUsersByFilter(filter);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
        }
    }
}
