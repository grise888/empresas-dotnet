﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.API.Services.Controllers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BR.Ioasys.IMDB.API.UnitTest.Application
{
    public class MovieAppUnitTest
    {
        private IMovieAppService _movieAppService;
        private readonly IMapper _mapper;

        public MovieAppUnitTest(IMovieAppService movieAppService, IMapper mapper)

        {
            _movieAppService = movieAppService;
            _mapper = mapper;
        }


        [Fact(DisplayName = "is it possible to insert a movie?")]
        public void InsertMovie()
        {
            MovieModel movie = new MovieModel()
            {
                Name = Faker.Name.FullName(),
            };
            MovieModel movieModel = _movieAppService.InsertMovie(movie);
            Assert.NotNull(movieModel);
            Assert.Equal(movieModel.Name, movie.Name);
        }


        [Fact(DisplayName = "is it possible to update a movie?")]
        public void UpdateMovie()
        {

            int movieId = 1;
            MovieModel movie = _movieAppService.GetMovie(movieId);
            movie.Name = "GODZILLA VS KONG";
            MovieModel movieModel = _movieAppService.UpdateMovie(movie);
            Assert.NotNull(movieModel);
            Assert.Equal(movieModel.Name, movie.Name);
        }


        [Fact(DisplayName = "is it possible to delete a movie?")]
        public void DeleteMovie()
        {

            int movieId = 7;
            MovieModel movieModel = _movieAppService.DeleteMovie(movieId);
            Assert.NotNull(movieModel);
        }

        [Fact(DisplayName = "is it possible to get a movie?")]
        public void GetMovie()
        {

            int movieId = 1;
            MovieModel movieModel = _movieAppService.GetMovie(movieId);
            Assert.NotNull(movieModel);
        }

        [Fact(DisplayName = "is it possible to get all movies?")]
        public void GetMovies()
        {
            List<MovieModel> movies = _movieAppService.GetMovies();
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }


        [Fact(DisplayName = "is it possible to get movies by director?")]
        public void GetMovieByDirector()
        {
            int directorId = 1;
            List<MovieModel> movies = _movieAppService.GetMovieByDirector(directorId);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }

        [Fact(DisplayName = "is it possible to get movies by gender?")]
        public void GetMovieByGender()
        {
            int genderId = 1;
            List<MovieModel> movies = _movieAppService.GetMovieByGender(genderId);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }


        [Fact(DisplayName = "is it possible to get movies by filter?")]
        public void GetMoviesByFilter()
        {
            MovieFilterModel filter = new MovieFilterModel()
            {
                Name = "Mulher",
                GenderName = "Ação"
            };
            List<MovieDetailModel> movies = _movieAppService.GetMoviesByFilter(filter);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
        }

        [Fact(DisplayName = "is it possible to detail movie?")]
        public void DetailMovie()
        {
            int movieId = 1;
            MovieDetailModel movie = _movieAppService.DetailMovie(movieId);
            Assert.NotNull(movie);
            Assert.True(movie.MovieId == 1);
        }
    }
}
