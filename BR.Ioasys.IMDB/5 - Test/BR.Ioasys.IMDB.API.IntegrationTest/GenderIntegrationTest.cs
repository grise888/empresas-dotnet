﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class GenderIntegrationTest : BaseIntegration
    {
        public GenderIntegrationTest() : base()
        {
        }

        [Fact]
        public async Task InserGender()
        {
            await AddToken();
            GenderModel genderModel = new GenderModel()
            {
                Name = "Chris Pratt"
            };
            var response = await PostJsonAsync(genderModel, $"{_hostApi}InsertGender", _httpClient);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var gender = JsonConvert.DeserializeObject<Gender>(data.Result);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(genderModel.Name, gender.Name);
            }

        }

        [Fact]
        public async Task UpdateGender()
        {
            await AddToken();
            GenderModel genderModel = new GenderModel()
            {
                GenderId = 1,
                Name = "Kyle Chandler",
            };
            var response = await PostJsonAsync(genderModel, $"{_hostApi}UpdateGender", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var gender = JsonConvert.DeserializeObject<Gender>(postResult);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(genderModel.Name, gender.Name);
            }
        }


        [Fact]
        public async Task DeleteUserGender()
        {
            await AddToken();
            int genderId = 1;
            var response = await PostJsonAsync(genderId, $"{_hostApi}DeleteGender", _httpClient);

            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var postResult = await response.Content.ReadAsStringAsync();
                var gender = JsonConvert.DeserializeObject<Gender>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetGenders()
        {
            int genderId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetGender?genderId=" + genderId);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var gender = JsonConvert.DeserializeObject<Gender>(jsonResult);
                Assert.NotNull(gender);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetGenderss()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetUsersGenders");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var genders = JsonConvert.DeserializeObject<List<Gender>>(jsonResult);
                Assert.NotNull(genders);
                Assert.True(genders.Count > 0);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }
    }
}
