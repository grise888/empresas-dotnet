﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class DirectorIntegrationTest : BaseIntegration
    {
        public DirectorIntegrationTest() : base()
        {
        }

        [Fact]
        public async Task InsertDirector()
        {
            await AddToken();
            DirectorModel directorModel = new DirectorModel()
            {
                Name = "Chris Pratt"
            };
            var response = await PostJsonAsync(directorModel, $"{_hostApi}InsertDirector", _httpClient);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var director = JsonConvert.DeserializeObject<Director>(data.Result);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(directorModel.Name, director.Name);
            }

        }

        [Fact]
        public async Task UpdateDirector()
        {
            await AddToken();
            DirectorModel directorModel = new DirectorModel()
            {
                DirectorId = 1,
                Name = "Kyle Chandler",
            };
            var response = await PostJsonAsync(directorModel, $"{_hostApi}UpdateDirector", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var director = JsonConvert.DeserializeObject<Director>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(directorModel.Name, director.Name);
            }
        }


        [Fact]
        public async Task DeleteDirector()
        {
            await AddToken();
            int directorId = 1;
            var response = await PostJsonAsync(directorId, $"{_hostApi}DeleteDirector", _httpClient);

            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var postResult = await response.Content.ReadAsStringAsync();
                var director = JsonConvert.DeserializeObject<Director>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetDirector()
        {
            int directorId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetDirector?directorId=" + directorId);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var director = JsonConvert.DeserializeObject<Director>(jsonResult);
                Assert.NotNull(director);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetDirectors()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetDirectors");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var directors = JsonConvert.DeserializeObject<List<Director>>(jsonResult);
                Assert.NotNull(directors);
                Assert.True(directors.Count > 0);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }
    }
}
