﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class UserStatusIntegrationTest : BaseIntegration
    {
        public UserStatusIntegrationTest() : base()
        {
        }

        [Fact]
        public async Task InsertUserStatus()
        {
            await AddToken();
            UserStatusModel userStatusModel = new UserStatusModel()
            {
                Name = "Chris Pratt"
            };
            var response = await PostJsonAsync(userStatusModel, $"{_hostApi}InsertUserStatus", _httpClient);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var userStatus = JsonConvert.DeserializeObject<UserStatus>(data.Result);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(userStatusModel.Name, userStatus.Name);
            }

        }

        [Fact]
        public async Task UpdateUserStatus()
        {
            await AddToken();
            UserStatusModel userStatusModel = new UserStatusModel()
            {
                StatusId = 1,
                Name = "Kyle Chandler",
            };
            var response = await PostJsonAsync(userStatusModel, $"{_hostApi}UpdateUserStatus", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userStatus = JsonConvert.DeserializeObject<UserStatus>(postResult);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(userStatusModel.Name, userStatus.Name);
            }
        }


        [Fact]
        public async Task DeleteUserStatus()
        {
            await AddToken();
            int userStatusId = 1;
            var response = await PostJsonAsync(userStatusId, $"{_hostApi}DeleteUserStatus", _httpClient);

            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var postResult = await response.Content.ReadAsStringAsync();
                var userStatus = JsonConvert.DeserializeObject<UserStatus>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetUserStatus()
        {
            int userStatusId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserStatus?userStatusId=" + userStatusId);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var userStatus = JsonConvert.DeserializeObject<UserStatus>(jsonResult);
                Assert.NotNull(userStatus);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetUsersStatus()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetUserStatuss");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var userStatuss = JsonConvert.DeserializeObject<List<UserStatus>>(jsonResult);
                Assert.NotNull(userStatuss);
                Assert.True(userStatuss.Count > 0);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }
    }
}
