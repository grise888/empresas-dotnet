﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Services;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Options;
using BR.Ioasys.IMDB.API.Application.AutoMapper;

namespace BR.Ioasys.IMDB.API.IntegrationTest.Core
{
    public abstract class BaseIntegration : IDisposable
    {
        public DatabaseContext _context { get; private set; }
        public HttpClient _httpClient { get; private set; }
        public IMapper _mapper { get; set; }
        public string _hostApi { get; set; }
        public HttpResponseMessage response { get; set; }

        public BaseIntegration()
        {
            _mapper = new AutoMapperFixture().GetMapper();
             _hostApi = GetUrl();
            _httpClient = GetHttpClient();
        }

        private string GetUrl()
        {
            var configuration = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json")
              .Build();

            var testConfiguration = new TestConfiguration();
            new ConfigureFromConfigurationOptions<TestConfiguration>(configuration.GetSection("Test"))
                .Configure(testConfiguration);


            var host = testConfiguration.Host;
            return host;
        }

        private HttpClient GetHttpClient()
        {
            
            var builder = new WebHostBuilder()
                   .UseEnvironment("Testing")
                   .UseStartup<Startup>();
            var server = new TestServer(builder);
            return server.CreateClient();
        }

        public async Task AddToken()
        {
            LoginModel login = new LoginModel()
            {
               Login = "grise",
               Password = "grise123"
            };

            var loginResul = await PostJsonAsync(login, _hostApi+ "Signin", _httpClient);
            var jsonLogin = await loginResul.Content.ReadAsStringAsync();
            var teste = JsonConvert.DeserializeObject("{\"success\":true,\"informations\":\"\",\"data\":{\"name\":\"Geraldo Grise\",\"email\":\"geraldo.grise@hotmail.com\",\"status\":\"Ativo\",\"profile\":\"Administrador\",\"token\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyJncmlzZSIsImdyaXNlIl0sImp0aSI6ImIxODllNjYyLTViYzctNDUyMS1iZWQwLWIzODFjOTA5MmEzZSIsIm5iZiI6MTYxOTk5NzkzMywiZXhwIjoxNjIwMjU5MjAwLCJpYXQiOjE2MTk5OTc5MzJ9.PyeWZPeEwrJcyaO3gajqPIEERgqnwbH2ULlgeUl4H_zhLIhD_AoHDimOKmI7OGDTuRPNtPIXHViIHMdomFdWylsKzKKQ - CBDVniVqJ4gQ4aNwDfS9IODosznVwbG_gEA9Ps0WLIbszdYLukU3rqyEuIACMZuFMsfLfUCgb9tYjp43DVJKxAwK6zK39MVQPb5iNJEITTEdz9MoRUEDvyRM6IE1T - wuzLFDA1WR2jnw53TqPtDUI9FTiDQF3V_ZgGpUzEYY5PY1t8pZfjMBKecD6mnobgdPg2fJ_k898vd0FTM0SPDNeSWMqHZJqhVoUC67y6Ys_FQTYJ2Y7Owc2ekoA\"}}");
            var response = JsonConvert.DeserializeObject<Response>(jsonLogin.Replace("[]","\"\""));
            var loginObject = JsonConvert.DeserializeObject<UserLoginModel>(response.Data.ToString());
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                                                         loginObject.Token);
        }

        public static async Task<HttpResponseMessage> PostJsonAsync(object dataclass, string url, HttpClient client)
        {
            return await client.PostAsync(url,
                new StringContent(JsonConvert.SerializeObject(dataclass), System.Text.Encoding.UTF8, "application/json"));
        }


        public async Task<string> GetJson(HttpResponseMessage response)
        {
            var postResult = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<Response>(postResult.Replace("[]", "\"\""));
            if (json == null) return string.Empty;
            return json.Data.ToString();
        }

        public class AutoMapperFixture : IDisposable
        {
            public IMapper GetMapper()
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new DomainToViewModelMappingProfile());
                    cfg.AddProfile(new ViewModelToDomainMappingProfile());

                });
                return config.CreateMapper();
            }
            public void Dispose() { }
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
