using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class ActorIntegrationTest : BaseIntegration
    {
        public ActorIntegrationTest(): base()
        {
        }

        [Fact]
        public async Task InsertActor()
        {
            await AddToken();
            ActorModel actorModel = new ActorModel()
            {
                Name = "Chris Pratt"
            };
            var response = await PostJsonAsync(actorModel, $"{_hostApi}InsertActor", _httpClient);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var actor = JsonConvert.DeserializeObject<Actor>(data.Result);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(actorModel.Name, actor.Name);
            }
           
        }

        [Fact]
        public async Task UpdateActor()
        {
            await AddToken();
            ActorModel actorModel = new ActorModel()
            {
                ActorId = 1,
                Name = "Kyle Chandler",
            };
            var response = await PostJsonAsync(actorModel, $"{_hostApi}UpdateActor", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var actor = JsonConvert.DeserializeObject<Actor>(postResult);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(actorModel.Name, actor.Name);
            }
        }


        [Fact]
        public async Task DeleteActor()
        {
            await AddToken();
            int actorId = 1;
            var response = await PostJsonAsync(actorId, $"{_hostApi}DeleteActor", _httpClient);
           
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var postResult = await response.Content.ReadAsStringAsync();
                var actor = JsonConvert.DeserializeObject<Actor>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetActor()
        {
            int actorId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetActor?actorId="+ actorId);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var actor = JsonConvert.DeserializeObject<Actor>(jsonResult);
                Assert.NotNull(actor);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetActors()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetActors");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var actors = JsonConvert.DeserializeObject<List<Actor>>(jsonResult);
                Assert.NotNull(actors);
                Assert.True(actors.Count > 0);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }
    }
}
