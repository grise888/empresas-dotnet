﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class UserProfileIntegrationTest : BaseIntegration
    {
        public UserProfileIntegrationTest() : base()
        {
        }

        [Fact]
        public async Task InsertUserProfile()
        {
            await AddToken();
            UserProfileModel userProfileModel = new UserProfileModel()
            {
                Name = "Chris Pratt"
            };
            var response = await PostJsonAsync(userProfileModel, $"{_hostApi}InsertUserProfile", _httpClient);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var userProfile = JsonConvert.DeserializeObject<UserProfile>(data.Result);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(userProfileModel.Name, userProfile.Name);
            }

        }

        [Fact]
        public async Task UpdateUserProfile()
        {
            await AddToken();
            UserProfileModel userProfileModel = new UserProfileModel()
            {
                ProfileId = 1,
                Name = "Kyle Chandler",
            };
            var response = await PostJsonAsync(userProfileModel, $"{_hostApi}UpdateUserProfile", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userProfile = JsonConvert.DeserializeObject<UserProfile>(postResult);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
                Assert.Equal(userProfileModel.Name, userProfile.Name);
            }
        }


        [Fact]
        public async Task DeleteUserProfile()
        {
            await AddToken();
            int userProfileId = 1;
            var response = await PostJsonAsync(userProfileId, $"{_hostApi}DeleteUserProfile", _httpClient);

            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var postResult = await response.Content.ReadAsStringAsync();
                var userProfile = JsonConvert.DeserializeObject<UserProfile>(postResult);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetUserProfile()
        {
            int userProfileId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserProfile?userProfileId=" + userProfileId);
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var jsonResult = await response.Content.ReadAsStringAsync();
                var userProfile = JsonConvert.DeserializeObject<UserProfile>(jsonResult);
                Assert.NotNull(userProfile);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Fact]
        public async Task GetUsersProfile()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetUserProfiles");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var data = GetJson(response);
            if (data.Result != string.Empty)
            {
                var userProfiles = JsonConvert.DeserializeObject<List<UserProfile>>(jsonResult);
                Assert.NotNull(userProfiles);
                Assert.True(userProfiles.Count > 0);
                Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            }
        }
    }
}
