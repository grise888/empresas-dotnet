﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class UserProfileIntegrationTest : BaseIntegration
    {
        public UserProfileIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertUserProfile()
        {
            await AddToken();
            UserProfileModel userProfileModel = new UserProfileModel()
            {
                Name = "Editor"
            };
            var response = await PostJsonAsync(userProfileModel, $"{_hostApi}InsertUserProfile", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userProfile = JsonConvert.DeserializeObject<UserProfile>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userProfileModel.Name, userProfile.Name);
        }

        [Fact]
        public async Task UpdateUserProfile()
        {
            await AddToken();
            UserProfileModel userProfileModel = new UserProfileModel()
            {
                ProfileId = 1,
                Name = "Administrador",
            };
            var response = await PostJsonAsync(userProfileModel, $"{_hostApi}UpdateUserProfile", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userProfile = JsonConvert.DeserializeObject<UserProfile>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userProfileModel.Name, userProfile.Name);
        }


        [Fact]
        public async Task DeleteUserProfile()
        {
            await AddToken();
            int userProfileId = 1;
            var response = await PostJsonAsync(userProfileId, $"{_hostApi}DeleteUserProfile", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userProfile = JsonConvert.DeserializeObject<UserProfile>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUserProfile()
        {
            int userProfileId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserProfile?userProfileId=" + userProfileId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var userProfile = JsonConvert.DeserializeObject<UserProfile>(jsonResult);
            Assert.NotNull(userProfile);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetProfiles()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetProfiles");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var userProfiles = JsonConvert.DeserializeObject<List<UserProfile>>(jsonResult);
            Assert.NotNull(userProfiles);
            Assert.True(userProfiles.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
