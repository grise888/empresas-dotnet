﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class DirectorIntegrationTest : BaseIntegration
    {
        public DirectorIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertDirector()
        {
            await AddToken();
            DirectorModel directorModel = new DirectorModel()
            {
                Name = "Joss Whedon"
            };
            var response = await PostJsonAsync(directorModel, $"{_hostApi}InsertDirector", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var director = JsonConvert.DeserializeObject<Director>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(directorModel.Name, director.Name);
        }

        [Fact]
        public async Task UpdateDirector()
        {
            await AddToken();
            DirectorModel directorModel = new DirectorModel()
            {
                DirectorId = 1,
                Name = "Adam Wingard",
            };
            var response = await PostJsonAsync(directorModel, $"{_hostApi}UpdateDirector", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var director = JsonConvert.DeserializeObject<Director>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(directorModel.Name, director.Name);
        }


        [Fact]
        public async Task DeleteDirector()
        {
            await AddToken();
            int directorId = 1;
            var response = await PostJsonAsync(directorId, $"{_hostApi}DeleteDirector", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var director = JsonConvert.DeserializeObject<Director>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetDirector()
        {
            int directorId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetDirector?directorId=" + directorId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var director = JsonConvert.DeserializeObject<Director>(jsonResult);
            Assert.NotNull(director);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetDirectors()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetDirectors");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var directors = JsonConvert.DeserializeObject<List<Director>>(jsonResult);
            Assert.NotNull(directors);
            Assert.True(directors.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
