﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class MovieIntegrationTest : BaseIntegration
    {
        public MovieIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertMovie()
        {
            await AddToken();
            MovieModel movieModel = new MovieModel()
            {
                Name = "Joss Whedon"
            };
            var response = await PostJsonAsync(movieModel, $"{_hostApi}InsertMovie", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(movieModel.Name, movie.Name);
        }

        [Fact]
        public async Task UpdateMovie()
        {
            await AddToken();
            MovieModel movieModel = new MovieModel()
            {
                MovieId = 1,
                Name = "Adam Wingard",
            };
            var response = await PostJsonAsync(movieModel, $"{_hostApi}UpdateMovie", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(movieModel.Name, movie.Name);
        }


        [Fact]
        public async Task DeleteMovie()
        {
            await AddToken();
            int movieId = 1;
            var response = await PostJsonAsync(movieId, $"{_hostApi}DeleteMovie", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetMovie()
        {
            int movieId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetMovie?movieId=" + movieId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(jsonResult);
            Assert.NotNull(movie);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task DetailMovie()
        {
            int movieId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}DetailMovie?movieId=" + movieId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<MovieDetailModel>(jsonResult);
            Assert.NotNull(movie);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetMovieByDirector()
        {
            int directorId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetMovieByDirector?directorId=" + directorId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(jsonResult);
            Assert.NotNull(movie);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetMovieByGender()
        {
            int genderId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetMovieByGender?genderId=" + genderId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movie = JsonConvert.DeserializeObject<Movie>(jsonResult);
            Assert.NotNull(movie);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetMovies()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetMovies");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movies = JsonConvert.DeserializeObject<List<Movie>>(jsonResult);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetMoviesByFilter()
        {
            MovieFilterModel filter = new MovieFilterModel()
            {
                Name = "Mulher"
            };
            var response = await PostJsonAsync(filter, $"{_hostApi}GetMoviesByFilter", _httpClient);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var movies = JsonConvert.DeserializeObject<List<MovieDetailModel>>(jsonResult);
            Assert.NotNull(movies);
            Assert.True(movies.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
