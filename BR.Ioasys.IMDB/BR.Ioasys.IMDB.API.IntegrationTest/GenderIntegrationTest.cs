﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class GenderIntegrationTest : BaseIntegration
    {
        public GenderIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertGender()
        {
            await AddToken();
            GenderModel actorModel = new GenderModel()
            {
                Name = "Documentário"
            };
            var response = await PostJsonAsync(actorModel, $"{_hostApi}InsertGender", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var actor = JsonConvert.DeserializeObject<Gender>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(actorModel.Name, actor.Name);
        }

        [Fact]
        public async Task UpdateGender()
        {
            await AddToken();
            GenderModel actorModel = new GenderModel()
            {
                GenderId = 1,
                Name = "Ação",
            };
            var response = await PostJsonAsync(actorModel, $"{_hostApi}UpdateGender", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var actor = JsonConvert.DeserializeObject<Gender>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(actorModel.Name, actor.Name);
        }


        [Fact]
        public async Task DeleteGender()
        {
            await AddToken();
            int genderId = 1;
            var response = await PostJsonAsync(genderId, $"{_hostApi}DeleteGender", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var actor = JsonConvert.DeserializeObject<Gender>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetGender()
        {
            int genderId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetGender?genderId=" + genderId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var actor = JsonConvert.DeserializeObject<Gender>(jsonResult);
            Assert.NotNull(actor);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetGenres()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetGenres");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var actors = JsonConvert.DeserializeObject<List<Gender>>(jsonResult);
            Assert.NotNull(actors);
            Assert.True(actors.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
