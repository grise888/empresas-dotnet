﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class UserStatusIntegrationTest : BaseIntegration
    {
        public UserStatusIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertUserStatus()
        {
            await AddToken();
            UserStatusModel userStatusModel = new UserStatusModel()
            {
                Name = "Cancelado"
            };
            var response = await PostJsonAsync(userStatusModel, $"{_hostApi}InsertUserStatus", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userStatus = JsonConvert.DeserializeObject<UserStatus>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userStatusModel.Name, userStatus.Name);
        }

        [Fact]
        public async Task UpdateUserStatus()
        {
            await AddToken();
            UserStatusModel userStatusModel = new UserStatusModel()
            {
                StatusId = 1,
                Name = "Ativo",
            };
            var response = await PostJsonAsync(userStatusModel, $"{_hostApi}UpdateUserStatus", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userStatus = JsonConvert.DeserializeObject<UserStatus>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userStatusModel.Name, userStatus.Name);
        }


        [Fact]
        public async Task DeleteUserStatus()
        {
            await AddToken();
            int userStatusId = 1;
            var response = await PostJsonAsync(userStatusId, $"{_hostApi}DeleteUserStatus", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var userStatus = JsonConvert.DeserializeObject<UserStatus>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUserStatus()
        {
            int userStatusId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserStatus?userStatusId=" + userStatusId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var userStatus = JsonConvert.DeserializeObject<UserStatus>(jsonResult);
            Assert.NotNull(userStatus);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUsersStatus()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetUsersStatus");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var userStatuss = JsonConvert.DeserializeObject<List<UserStatus>>(jsonResult);
            Assert.NotNull(userStatuss);
            Assert.True(userStatuss.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
