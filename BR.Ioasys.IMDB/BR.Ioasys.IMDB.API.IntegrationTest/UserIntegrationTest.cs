﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.IntegrationTest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BR.Ioasys.IMDB.API.IntegrationTest
{
    public class UserIntegrationTest : BaseIntegration
    {
        public UserIntegrationTest(IMapper mapper) : base(mapper)
        {

        }

        [Fact]
        public async Task InsertUser()
        {
            await AddToken();
            UserModel userModel = new UserModel()
            {
                Name = "Joss Whedon"
            };
            var response = await PostJsonAsync(userModel, $"{_hostApi}InsertUser", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userModel.Name, user.Name);
        }

        [Fact]
        public async Task UpdateUser()
        {
            await AddToken();
            UserModel userModel = new UserModel()
            {
                UserId = 1,
                Name = "Adam Wingard",
            };
            var response = await PostJsonAsync(userModel, $"{_hostApi}UpdateUser", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(userModel.Name, user.Name);
        }


        [Fact]
        public async Task DeleteUser()
        {
            await AddToken();
            int userId = 1;
            var response = await PostJsonAsync(userId, $"{_hostApi}DeleteUser", _httpClient);
            var postResult = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(postResult);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUser()
        {
            int userId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUser?userId=" + userId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(jsonResult);
            Assert.NotNull(user);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUserByProfile()
        {
            int profileId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserByProfile?profileId=" + profileId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<User>>(jsonResult);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUserByStatus()
        {
            int statusId = 1;
            var response = await _httpClient.GetAsync($"{_hostApi}GetUserByStatus?statusId=" + statusId);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<User>>(jsonResult);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetActiveClients()
        {
            UserFilterModel filter = new UserFilterModel()
            {
                Page = 1,
                Pagination = true,
                NumberRecords = 10
            };
            var response = await PostJsonAsync(filter, $"{_hostApi}GetActiveClients", _httpClient);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<User>>(jsonResult);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUsers()
        {

            var response = await _httpClient.GetAsync($"{_hostApi}GetUsers");
            var jsonResult = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<User>>(jsonResult);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task GetUsersByFilter()
        {
            UserModel filter = new UserModel()
            {
                Name = "Geraldo"
            };
            var response = await PostJsonAsync(filter, $"{_hostApi}GetUsersByFilter", _httpClient);
            var jsonResult = await response.Content.ReadAsStringAsync();
            var users = JsonConvert.DeserializeObject<List<User>>(jsonResult);
            Assert.NotNull(users);
            Assert.True(users.Count > 0);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
    }
}
