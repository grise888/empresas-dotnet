﻿using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Services;
using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BR.Ioasys.IMDB.API.IntegrationTest.Core
{
    public abstract class BaseIntegration : IDisposable
    {
        public DatabaseContext _context { get; private set; }
        public HttpClient _httpClient { get; private set; }
        public IMapper _mapper { get; set; }
        public string _hostApi { get; set; }
        public HttpResponseMessage response { get; set; }

        public BaseIntegration(IMapper mapper)
        {
            _mapper = mapper;
             _hostApi = "http://localhost:5000/api/";
            _httpClient = GetHttpClient();
        }

        private HttpClient GetHttpClient()
        {
            
            var builder = new WebHostBuilder()
                   .UseEnvironment("Testing")
                   .UseStartup<Startup>();
            var server = new TestServer(builder);
            return server.CreateClient();
        }

        public async Task AddToken()
        {
            LoginModel login = new LoginModel()
            {
               Login = "grise",
               Password = "grise123"
            };

            var loginResul = await PostJsonAsync(login, _hostApi, _httpClient);
            var jsonLogin = await loginResul.Content.ReadAsStringAsync();
            var loginObject = JsonConvert.DeserializeObject<UserLoginModel>(jsonLogin);

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                                                         loginObject.Token);
        }

        public static async Task<HttpResponseMessage> PostJsonAsync(object dataclass, string url, HttpClient client)
        {
            return await client.PostAsync(url,
                new StringContent(JsonConvert.SerializeObject(dataclass), System.Text.Encoding.UTF8, "application/json"));
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
