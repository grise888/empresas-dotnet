using MediatR;
using Microsoft.Extensions.DependencyInjection;

using BR.Ioasys.IMDB.API.Application.Apps;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using BR.Ioasys.IMDB.Infra.CrossCutting.Log;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Repository;

using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Repository;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Repository;

using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login.Services;

namespace BR.Ioasys.IMDB.Infra.CrossCutting.Ioc
{
    public static class BootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // Application
			services.AddScoped<IActorAppService,ActorAppService >();
			services.AddScoped<IDirectorAppService,DirectorAppService >();
			services.AddScoped<IGenderAppService,GenderAppService >();
			services.AddScoped<IMovieAppService,MovieAppService >();
			services.AddScoped<IMovieActorsAppService,MovieActorsAppService >();
			services.AddScoped<IMovieNotesAppService,MovieNotesAppService >();
			services.AddScoped<IUserAppService,UserAppService >();
			services.AddScoped<IUserProfileAppService,UserProfileAppService >();
			services.AddScoped<IUserStatusAppService,UserStatusAppService >();
            // Integração
			
            // Domain
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            // Service
    	    services.AddSingleton<ILogger, Logger>();
			services.AddScoped<IActorService,ActorService >();
			services.AddScoped<IDirectorService,DirectorService >();
			services.AddScoped<IGenderService,GenderService >();
			services.AddScoped<ILoginService, LoginService>();
			services.AddScoped<IMovieService,MovieService >();
			services.AddScoped<IMovieActorsService,MovieActorsService >();
			services.AddScoped<IMovieNotesService,MovieNotesService >();
			services.AddScoped<IUserService,UserService >();
			services.AddScoped<IUserProfileService,UserProfileService >();
			services.AddScoped<IUserStatusService,UserStatusService >();
            // Data
			services.AddScoped<DatabaseContext>();
			services.AddScoped<IActorRepository,ActorRepository >();
			services.AddScoped<IDirectorRepository,DirectorRepository >();
			services.AddScoped<IGenderRepository,GenderRepository >();
			services.AddScoped<IMovieRepository,MovieRepository >();
			services.AddScoped<IMovieActorsRepository,MovieActorsRepository >();
			services.AddScoped<IMovieNotesRepository,MovieNotesRepository >();
			services.AddScoped<IUserRepository,UserRepository >();
			services.AddScoped<IUserProfileRepository,UserProfileRepository >();
			services.AddScoped<IUserStatusRepository,UserStatusRepository >();
		}
	}
}