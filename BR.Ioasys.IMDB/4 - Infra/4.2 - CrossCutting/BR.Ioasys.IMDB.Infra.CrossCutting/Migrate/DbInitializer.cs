﻿using BR.Ioasys.IMDB.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.CrossCutting.Migrate
{
    public static class DbInitializer
    {
        public static void Initialize()
        {
            DatabaseContext context = new DatabaseContext();
            Migrate(context);
        }

        private static bool CheckTableExists(DatabaseContext context)
        {
            try
            {
                context.Users.ToList();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private static void Migrate(DatabaseContext context)
        {
            string backup = string.Empty;
            bool exists = CheckTableExists(context);
            if (exists)
            {
                string path = Path.GetFullPath("SQL\\backup.sql");
                using (StreamReader sr = new StreamReader(path))
                {
                    string query = sr.ReadToEnd();
                    query = query.Replace("GO", string.Empty);
                    if (!exists)
                    {
                        context.Database.ExecuteSqlRaw(@"" + query);
                    }
                }
            }
           
        }
    }
}

