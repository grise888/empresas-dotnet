using BR.Ioasys.IMDB.Infra.Data.Core.Extensions;
using BR.Ioasys.IMDB.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;

namespace BR.Ioasys.IMDB.Infra.Data.Context
{
    public class DatabaseContext : DbContext
    {
        
		public DbSet<Actor> Actors { get; set; }	
		public DbSet<Director> Directors { get; set; }	
		public DbSet<Gender> Genres { get; set; }	
		public DbSet<Movie> Movies { get; set; }	
		public DbSet<MovieActors> MoviesActors { get; set; }	
		public DbSet<MovieNotes> MoviesNotes { get; set; }	
		public DbSet<User> Users { get; set; }	
		public DbSet<UserProfile> Profiles { get; set; }	
		public DbSet<UserStatus> UsersStatus { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
			modelBuilder.AddConfiguration(new ActorMap());	
			modelBuilder.AddConfiguration(new DirectorMap());	
			modelBuilder.AddConfiguration(new GenderMap());	
			modelBuilder.AddConfiguration(new MovieMap());	
			modelBuilder.AddConfiguration(new MovieActorsMap());	
			modelBuilder.AddConfiguration(new MovieNotesMap());	
			modelBuilder.AddConfiguration(new UserMap());	
			modelBuilder.AddConfiguration(new UserProfileMap());	
			modelBuilder.AddConfiguration(new UserStatusMap());	
            base.OnModelCreating(modelBuilder);
        }
		
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.EnableSensitiveDataLogging(false);
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"), opt => opt.CommandTimeout(120));
        }
    }
}