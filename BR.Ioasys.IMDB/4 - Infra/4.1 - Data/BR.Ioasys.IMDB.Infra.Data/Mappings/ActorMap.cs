using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class ActorMap : MappingCore<Actor>
    {
        public override void Map(EntityTypeBuilder<Actor> builder)
        {
            builder.ToTable("TBLACT");
			builder.HasKey(x => x.ActorId);
			builder.Property(x => x.ActorId).HasColumnName(@"ACT_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"ACT_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
            base.Map(builder);
        }
    }
}