using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class GenderMap : MappingCore<Gender>
    {
        public override void Map(EntityTypeBuilder<Gender> builder)
        {
            builder.ToTable("TBLGEN");
			builder.HasKey(x => x.GenderId);
			builder.Property(x => x.GenderId).HasColumnName(@"GEN_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"GEN_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
            base.Map(builder);
        }
    }
}