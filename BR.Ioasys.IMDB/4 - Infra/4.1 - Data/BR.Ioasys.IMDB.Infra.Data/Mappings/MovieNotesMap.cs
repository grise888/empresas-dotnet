using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class MovieNotesMap : MappingCore<MovieNotes>
    {
        public override void Map(EntityTypeBuilder<MovieNotes> builder)
        {
            builder.ToTable("TBLMNTE");
			builder.HasKey(x => x.MovieNoteId);
			builder.Property(x => x.MovieNoteId).HasColumnName(@"MNTE_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.MovieId).HasColumnName(@"MOV_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.Movie).WithMany(y => y.MovieNotesList).HasForeignKey(x => x.MovieId);
			builder.Property(x => x.UserId).HasColumnName(@"USER_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.User).WithMany(y => y.MovieNotesList).HasForeignKey(x => x.UserId);
			builder.Property(x => x.Note).HasColumnName(@"MNTE_NT").IsRequired().HasColumnType("int");
            base.Map(builder);
        }
    }
}