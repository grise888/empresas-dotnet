using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class UserStatusMap : MappingCore<UserStatus>
    {
        public override void Map(EntityTypeBuilder<UserStatus> builder)
        {
            builder.ToTable("TBLSTA");
			builder.HasKey(x => x.StatusId);
			builder.Property(x => x.StatusId).HasColumnName(@"STA_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"STA_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
            base.Map(builder);
        }
    }
}