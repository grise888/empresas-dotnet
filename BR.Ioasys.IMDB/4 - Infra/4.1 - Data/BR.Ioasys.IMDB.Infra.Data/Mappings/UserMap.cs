using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class UserMap : MappingCore<User>
    {
        public override void Map(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("TBLUSER");
			builder.HasKey(x => x.UserId);
			builder.Property(x => x.UserId).HasColumnName(@"USER_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"USER_NM").IsRequired().HasColumnType("varchar").HasMaxLength(250);
			builder.Property(x => x.Email).HasColumnName(@"USER_EMl").IsRequired().HasColumnType("varchar").HasMaxLength(250);
			builder.Property(x => x.Login).HasColumnName(@"USER_LG").IsRequired().HasColumnType("varchar").HasMaxLength(150);
			builder.Property(x => x.Password).HasColumnName(@"USER_PW").IsRequired().HasColumnType("varchar").HasMaxLength(50);
			builder.Property(x => x.StatusId).HasColumnName(@"STA_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.UserStatus).WithMany(y => y.UserList).HasForeignKey(x => x.StatusId);
			builder.Property(x => x.ProfileId).HasColumnName(@"PRO_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.UserProfile).WithMany(y => y.UserList).HasForeignKey(x => x.ProfileId);
            base.Map(builder);
        }
    }
}