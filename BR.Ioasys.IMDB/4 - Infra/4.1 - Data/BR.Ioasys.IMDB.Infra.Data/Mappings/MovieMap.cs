using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class MovieMap : MappingCore<Movie>
    {
        public override void Map(EntityTypeBuilder<Movie> builder)
        {
            builder.ToTable("TBLMOV");
			builder.HasKey(x => x.MovieId);
			builder.Property(x => x.MovieId).HasColumnName(@"MOV_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"MOV_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
			builder.Property(x => x.Detail).HasColumnName(@"MOV_DTL").IsRequired().HasColumnType("varchar").HasMaxLength(400);
			builder.Property(x => x.GenderId).HasColumnName(@"GEN_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.Gender).WithMany(y => y.MovieList).HasForeignKey(x => x.GenderId);
			builder.Property(x => x.DirectorId).HasColumnName(@"DIR_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.Director).WithMany(y => y.MovieList).HasForeignKey(x => x.DirectorId);
            base.Map(builder);
        }
    }
}