using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class UserProfileMap : MappingCore<UserProfile>
    {
        public override void Map(EntityTypeBuilder<UserProfile> builder)
        {
            builder.ToTable("TBLPRO");
			builder.HasKey(x => x.ProfileId);
			builder.Property(x => x.ProfileId).HasColumnName(@"PRO_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"PRO_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
            base.Map(builder);
        }
    }
}