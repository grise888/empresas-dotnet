using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class DirectorMap : MappingCore<Director>
    {
        public override void Map(EntityTypeBuilder<Director> builder)
        {
            builder.ToTable("TBLDIR");
			builder.HasKey(x => x.DirectorId);
			builder.Property(x => x.DirectorId).HasColumnName(@"DIR_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.Name).HasColumnName(@"DIR_NM").IsRequired().HasColumnType("varchar").HasMaxLength(255);
            base.Map(builder);
        }
    }
}