using BR.Ioasys.IMDB.Infra.Data.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;

namespace BR.Ioasys.IMDB.Infra.Data.Mappings
{

    public class MovieActorsMap : MappingCore<MovieActors>
    {
        public override void Map(EntityTypeBuilder<MovieActors> builder)
        {
            builder.ToTable("TBLMVAC");
			builder.HasKey(x => x.MovieActorId);
			builder.Property(x => x.MovieActorId).HasColumnName(@"MVAC_ID").IsRequired().HasColumnType("int");
			builder.Property(x => x.MovieId).HasColumnName(@"MOV_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.Movie).WithMany(y => y.MovieActorsList).HasForeignKey(x => x.MovieId);
			builder.Property(x => x.ActorId).HasColumnName(@"ACT_ID").IsRequired().HasColumnType("int");
			builder.HasOne(x => x.Actor).WithMany(y => y.MovieActorsList).HasForeignKey(x => x.ActorId);
            base.Map(builder);
        }
    }
}