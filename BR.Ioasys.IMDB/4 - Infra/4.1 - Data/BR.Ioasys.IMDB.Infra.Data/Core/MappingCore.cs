using BR.Ioasys.IMDB.API.Domain.Aggregates.Core;
using BR.Ioasys.IMDB.Infra.Data.Core.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.Infra.Data.Core
{
    public class MappingCore<T> : EntityTypeConfiguration<T> where T : EntityCore<T>
    {
        public override void Map(EntityTypeBuilder<T> builder)
        {
            builder.Ignore(x => x.CascadeMode);
            builder.Ignore(x => x.Pagination);
            builder.Ignore(x => x.Page);
            builder.Ignore(x => x.NumberRecords);
            builder.Ignore(x => x.ValidationResult);
        }
    }

}
