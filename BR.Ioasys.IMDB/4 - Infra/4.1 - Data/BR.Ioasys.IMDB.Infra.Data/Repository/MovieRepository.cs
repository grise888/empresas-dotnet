using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(DatabaseContext context) : base(context)
        {

        }
		
		public List<Movie> GetMovieByGender(int genderId)
		{
			return _context.Movies.Where(x => x.GenderId == genderId).ToList();
		}

		public List<Movie> GetMovieByDirector(int directorId)
		{
			return _context.Movies.Where(x => x.DirectorId == directorId).ToList();
		}

		public List<Movie> GetMoviesByFilter(Movie movie)
		{
			var movies = _context.Movies.Include("Gender").Include("Director").Where(
				   x => (
		     		 movie.Name == null || x.Name.Contains(movie.Name)
					 && movie.GenderName == null || x.Gender.Name.Contains(movie.GenderName)
					 && movie.DirectorName == null || x.Director.Name.Contains(movie.DirectorName)
				   )
			).Select(x=> new Movie() { 
				MovieId = x.MovieId,
			    Name = x.Name,
				Detail = x.Detail,
				GenderName = x.Gender.Name,
				DirectorName = x.Director.Name
			});

				  

			List<Movie> movieList = movies.ToList().OrderBy(x => x.Name).ToList();
			if (movie.MovieId != 0)
			{
				movieList = movieList.Where(x=> x.MovieId == movie.MovieId).ToList();
			}

			if (movie.Pagination)
			{
				movieList = movieList.Skip(movie.NumberRecords * (movie.Page - 1)).Take(movie.NumberRecords).ToList();
			}

			return movieList;
		}

		

    }
}