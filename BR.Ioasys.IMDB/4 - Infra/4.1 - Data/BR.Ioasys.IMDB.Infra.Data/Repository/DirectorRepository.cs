using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class DirectorRepository : Repository<Director>, IDirectorRepository
    {
        public DirectorRepository(DatabaseContext context) : base(context)
        {

        }
		
    }
}