using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class ActorRepository : Repository<Actor>, IActorRepository
    {
        public ActorRepository(DatabaseContext context) : base(context)
        {

        }
		
    }
}