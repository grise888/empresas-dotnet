using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DatabaseContext context) : base(context)
        {

        }

		public List<User> GetUserByUserProfile(int profileId)
		{
			return _context.Users.Where(x => x.ProfileId == profileId).ToList();
		}

		public List<User> GetUserByUserStatus(int statusId)
		{
			return _context.Users.Where(x => x.StatusId == statusId).ToList();
		}

		public List<User> GetUsersByFilter(User user)
		{
			return _context.Users.Where(x =>(x.UserId == 0 || x.UserId == user.UserId)
							&& (x.Name == null || x.Name == user.Name)
							&& (x.Email == null || x.Email == user.Email)
							&& (x.Login == null || x.Login == user.Login)
							&& (x.Password == null || x.Password == user.Password)
							&& (x.StatusId == 0 || x.StatusId == user.StatusId)
							&& (x.ProfileId == 0 || x.ProfileId == user.ProfileId)).ToList();
		}

		public List<User> GetActiveClients(UserFilter filter)
		{
			var userList =  _context.Users.
			   Where(x => x.StatusId == (int)Status.Ativo && x.ProfileId == (int)ProfileEnum.Cliente)
			   .OrderBy(x=> x.Name)
			   .ToList();

			if (filter.Pagination)
			{
				userList = userList.Skip(filter.NumberRecords * (filter.Page - 1)).Take(filter.NumberRecords).ToList();
			}

			return userList;
		}

		public UserLogin Signin(string login, string password)
		{

			var data = from user in _context.Users
					   join sta in _context.UsersStatus on user.StatusId equals sta.StatusId
					   join prof in _context.Profiles on user.ProfileId equals prof.ProfileId
					   where (user.Login.Equals(login) && user.Password.Equals(password))
					   select new UserLogin
					   {
						   Name = user.Name,
						   Email = user.Email,
						   Profile = prof.Name,
						   Status = sta.Name
					   };
			return data.FirstOrDefault();
		}
    }
}