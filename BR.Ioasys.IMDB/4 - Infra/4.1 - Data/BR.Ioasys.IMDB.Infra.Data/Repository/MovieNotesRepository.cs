using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class MovieNotesRepository : Repository<MovieNotes>, IMovieNotesRepository
    {
        public MovieNotesRepository(DatabaseContext context) : base(context)
        {

        }
		
		public List<MovieNotes> GetMovieNotesByMovie(int movieId)
		{
			return _context.MoviesNotes.Where(x => x.MovieId == movieId).ToList();
		}

		public List<MovieNotes> GetMovieNotesByUser(int userId)
		{
			return _context.MoviesNotes.Where(x => x.UserId == userId).ToList();
		}
    }
}