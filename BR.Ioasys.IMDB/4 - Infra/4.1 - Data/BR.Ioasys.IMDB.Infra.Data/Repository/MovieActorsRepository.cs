using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class MovieActorsRepository : Repository<MovieActors>, IMovieActorsRepository
    {
        public MovieActorsRepository(DatabaseContext context) : base(context)
        {

        }
		
		public List<MovieActors> GetMovieActorsByActor(int actorId)
		{
			return _context.MoviesActors.Where(x => x.ActorId == actorId).ToList();
		}

		public List<MovieActors> GetMovieActorsByMovie(int movieId)
		{
			return _context.MoviesActors.Include("Actor").Where(x => x.MovieId == movieId).ToList();
		}
    }
}