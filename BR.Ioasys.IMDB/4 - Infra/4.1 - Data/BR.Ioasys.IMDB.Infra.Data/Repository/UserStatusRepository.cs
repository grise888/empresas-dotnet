using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Repository;
using BR.Ioasys.IMDB.Infra.Data.Context;
using BR.Ioasys.IMDB.Infra.Data.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BR.Ioasys.IMDB.Infra.Data.Repository
{
    public class UserStatusRepository : Repository<UserStatus>, IUserStatusRepository
    {
        public UserStatusRepository(DatabaseContext context) : base(context)
        {

        }
		
    }
}