using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;


namespace BR.Ioasys.IMDB.API.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            
			CreateMap<Actor, ActorModel>();
			CreateMap<Director, DirectorModel>();
			CreateMap<Gender, GenderModel>();
			CreateMap<Movie, MovieModel>();
			CreateMap<Movie, MovieDetailModel>();
			CreateMap<Movie, MovieFilterModel>();
			CreateMap<MovieActors, MovieActorsModel>();
			CreateMap<MovieNotes, MovieNotesModel>();
			CreateMap<User, UserModel>();
			CreateMap<UserFilter, UserFilterModel>();
			CreateMap<UserLogin, UserLoginModel>();
			CreateMap<UserProfile, UserProfileModel>();
			CreateMap<UserStatus, UserStatusModel>();
        }
    }
}