using AutoMapper;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;


namespace BR.Ioasys.IMDB.API.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
		    
			CreateMap<ActorModel, Actor>();
			CreateMap<DirectorModel, Director>();
			CreateMap<GenderModel, Gender>();
			CreateMap<MovieModel, Movie>();
			CreateMap<MovieDetailModel, Movie>();
			CreateMap<MovieFilterModel, Movie>();
			CreateMap<MovieActorsModel, MovieActors>();
			CreateMap<MovieNotesModel, MovieNotes>();
			CreateMap<UserModel, User>();
			CreateMap<UserFilterModel, UserFilter>();
			CreateMap<UserProfileModel, UserProfile>();
			CreateMap<UserProfileModel, UserProfile>();
			CreateMap<UserStatusModel, UserStatus>();
        }
    }
}