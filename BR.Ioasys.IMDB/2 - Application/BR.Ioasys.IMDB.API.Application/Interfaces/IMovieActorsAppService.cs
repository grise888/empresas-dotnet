using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IMovieActorsAppService : IDisposable
    {
		
		public void InsertMovieActors(MovieActorsModel movieActors);

		public void UpdateMovieActors(MovieActorsModel movieActors);

		public void DeleteMovieActors(int movieActor_id);

		public MovieActorsModel GetMovieActors(int movieActor_id);

		public List<MovieActorsModel> GetMoviesActors();

		public List<MovieActorsModel> GetMovieActorsByActor(int actor_id);

		public List<MovieActorsModel> GetMovieActorsByMovie(int movie_id);

    }
}