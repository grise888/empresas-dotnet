using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IUserAppService : IDisposable
    {
		
		public UserModel InsertUser(UserModel user);

		public UserModel UpdateUser(UserModel user);

		public UserModel DeleteUser(int user_id);

		public UserModel ActivateUser(int user_id);

		public UserModel InactivateUser(int user_id);

		public UserModel GetUser(int user_id);

		public List<UserModel> GetUsers();

		public List<UserModel> GetUserByUserProfile(int profile_id);

		public List<UserModel> GetUserByUserStatus(int status_id);

		public List<UserModel> GetUsersByFilter(UserModel user);
		public List<UserModel> GetActiveClients(UserFilterModel user);
		public UserLoginModel Signin(string login, string password);

	}
}