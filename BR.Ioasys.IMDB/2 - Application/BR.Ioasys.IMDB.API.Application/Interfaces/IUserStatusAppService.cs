using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IUserStatusAppService : IDisposable
    {
		
		public UserStatusModel InsertUserStatus(UserStatusModel userStatus);

		public UserStatusModel UpdateUserStatus(UserStatusModel userStatus);

		public UserStatusModel DeleteUserStatus(int status_id);

		public UserStatusModel GetUserStatus(int status_id);

		public List<UserStatusModel> GetUsersStatus();

    }
}