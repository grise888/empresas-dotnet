using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IMovieNotesAppService : IDisposable
    {
		
		public void InsertMovieNotes(MovieNotesModel movieNotes);

		public void UpdateMovieNotes(MovieNotesModel movieNotes);

		public void DeleteMovieNotes(int movieNote_id);

		public MovieNotesModel GetMovieNotes(int movieNote_id);

		public List<MovieNotesModel> GetMoviesNotes();

		public List<MovieNotesModel> GetMovieNotesByMovie(int movie_id);

		public List<MovieNotesModel> GetMovieNotesByUser(int user_id);

    }
}