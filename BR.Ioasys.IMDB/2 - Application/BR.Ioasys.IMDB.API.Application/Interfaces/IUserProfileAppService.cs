using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IUserProfileAppService : IDisposable
    {
		
		public UserProfileModel InsertUserProfile(UserProfileModel userProfile);

		public UserProfileModel UpdateUserProfile(UserProfileModel userProfile);

		public UserProfileModel DeleteUserProfile(int profile_id);

		public UserProfileModel GetUserProfile(int profile_id);

		public List<UserProfileModel> GetProfiles();

    }
}