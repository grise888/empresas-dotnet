using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IDirectorAppService : IDisposable
    {
		
		public DirectorModel InsertDirector(DirectorModel director);

		public DirectorModel UpdateDirector(DirectorModel director);

		public DirectorModel DeleteDirector(int director_id);

		public DirectorModel GetDirector(int director_id);

		public List<DirectorModel> GetDirectors();

    }
}