using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IGenderAppService : IDisposable
    {
		
		public GenderModel InsertGender(GenderModel gender);

		public GenderModel UpdateGender(GenderModel gender);

		public GenderModel DeleteGender(int gender_id);

		public GenderModel GetGender(int gender_id);

		public List<GenderModel> GetGenres();

    }
}