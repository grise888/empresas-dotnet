using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IActorAppService : IDisposable
    {
		
		public ActorModel InsertActor(ActorModel actor);

		public ActorModel UpdateActor(ActorModel actor);

		public ActorModel DeleteActor(int actor_id);

		public ActorModel GetActor(int actor_id);

		public List<ActorModel> GetActors();

    }
}