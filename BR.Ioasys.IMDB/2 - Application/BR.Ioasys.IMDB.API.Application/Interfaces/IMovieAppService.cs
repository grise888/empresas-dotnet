using BR.Ioasys.IMDB.API.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Interfaces
{
    public interface IMovieAppService : IDisposable
    {
		
		public MovieModel InsertMovie(MovieModel movie);
		public MovieModel UpdateMovie(MovieModel movie);
		public MovieModel DeleteMovie(int movie_id);
		public MovieModel GetMovie(int movie_id);
		public List<MovieModel> GetMovies();
		public List<MovieModel> GetMovieByGender(int gender_id);
		public List<MovieModel> GetMovieByDirector(int director_id);
     	public List<MovieDetailModel> GetMoviesByFilter(MovieFilterModel movie);
		public MovieDetailModel DetailMovie(int movie_id);


	}
}