using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Gender.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class GenderAppService : ApplicationService, IGenderAppService
    {
        private readonly IMapper _mapper;

        private readonly IGenderService _genderService;

        public GenderAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IGenderService genderService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _genderService = genderService;
        }
		
		public GenderModel InsertGender(GenderModel gender)
		{
			var genderModel = _genderService.InsertGender(_mapper.Map<Gender>(gender));
			return _mapper.Map<GenderModel>(genderModel);
		}

		public GenderModel UpdateGender(GenderModel gender)
		{
			var genderModel =  _genderService.UpdateGender(_mapper.Map<Gender>(gender));
			return _mapper.Map<GenderModel>(genderModel);
		}

		public GenderModel DeleteGender(int genderId)
		{
			var genderModel = _genderService.DeleteGender(genderId);
			return _mapper.Map<GenderModel>(genderModel);
		}

		public GenderModel GetGender(int genderId)
		{
			return _mapper.Map<GenderModel>(_genderService.GetGender(genderId));
		}

		public List<GenderModel> GetGenres( )
		{
			return _mapper.Map<List<GenderModel>>(_genderService.GetGenres());
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_genderService.Dispose();
		}

    }
}