using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserStatus.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class UserStatusAppService : ApplicationService, IUserStatusAppService
    {
        private readonly IMapper _mapper;

        private readonly IUserStatusService _userStatusService;

        public UserStatusAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IUserStatusService userStatusService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _userStatusService = userStatusService;
        }
		
		public UserStatusModel InsertUserStatus(UserStatusModel userStatus)
		{
			var statusModel =  _userStatusService.InsertUserStatus(_mapper.Map<UserStatus>(userStatus));
			return _mapper.Map<UserStatusModel>(statusModel);
		}

		public UserStatusModel UpdateUserStatus(UserStatusModel userStatus)
		{
			var statusModel = _userStatusService.UpdateUserStatus(_mapper.Map<UserStatus>(userStatus));
			return _mapper.Map<UserStatusModel>(statusModel);
		}

		public UserStatusModel DeleteUserStatus(int statusId)
		{
			var statusModel = _userStatusService.DeleteUserStatus(statusId);
			return _mapper.Map<UserStatusModel>(statusModel);
		}

		public UserStatusModel GetUserStatus(int statusId)
		{
			return _mapper.Map<UserStatusModel>(_userStatusService.GetUserStatus(statusId));
		}

		public List<UserStatusModel> GetUsersStatus( )
		{
			return _mapper.Map<List<UserStatusModel>>(_userStatusService.GetUsersStatus());
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_userStatusService.Dispose();
		}

    }
}