using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Login.Services;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.User.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class UserAppService : ApplicationService, IUserAppService
    {
        private readonly IMapper _mapper;
		private readonly ILoginService _loginService;
		private readonly IUserService _userService;
	

		public UserAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
							   ILoginService loginService,
							   IUserService userService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _userService = userService;
			_loginService = loginService;
		}
		
		public UserModel InsertUser(UserModel user)
		{
			var userModel = _userService.InsertUser(_mapper.Map<User>(user));
			return _mapper.Map<UserModel>(userModel);
		}

		public UserModel UpdateUser(UserModel user)
		{
		   	var userModel =  _userService.UpdateUser(_mapper.Map<User>(user));
			return _mapper.Map<UserModel>(userModel);
		}

		public UserModel DeleteUser(int userId)
		{
			var userModel =  _userService.InactivateUser(userId);
			return _mapper.Map<UserModel>(userModel);
		}

		public UserModel ActivateUser(int userId)
		{
			var userModel =  _userService.ActivateUser(userId);
			return _mapper.Map<UserModel>(userModel);
		}

		public UserModel InactivateUser(int userId)
		{
			var userModel =  _userService.InactivateUser(userId);
			return _mapper.Map<UserModel>(userModel);
		}


		public UserModel GetUser(int userId)
		{
			return _mapper.Map<UserModel>(_userService.GetUser(userId));
		}

		public List<UserModel> GetUsers( )
		{
			return _mapper.Map<List<UserModel>>(_userService.GetUsers());
		}

		public List<UserModel> GetUserByUserProfile(int profileId)
		{
			return _mapper.Map<List<UserModel>>(_userService.GetUserByUserProfile(profileId));
		}

		public List<UserModel> GetUserByUserStatus(int statusId)
		{
			return _mapper.Map<List<UserModel>>(_userService.GetUserByUserStatus(statusId));
		}

		public List<UserModel> GetUsersByFilter(UserModel user)
		{
			return _mapper.Map<List<UserModel>>(_userService.GetUsersByFilter(_mapper.Map<User>(user)));
		}

		public List<UserModel> GetActiveClients(UserFilterModel user)
		{
			return _mapper.Map<List<UserModel>>(_userService.GetActiveClients(_mapper.Map<UserFilter>(user)));
		}



		public UserLoginModel Signin(string login, string password)
		{
			return _mapper.Map<UserLoginModel>(_loginService.Signin(login, password));
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_userService.Dispose();
		}

    }
}