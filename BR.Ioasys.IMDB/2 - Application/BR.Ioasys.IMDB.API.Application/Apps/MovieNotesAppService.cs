using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieNotes.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class MovieNotesAppService : ApplicationService, IMovieNotesAppService
    {
        private readonly IMapper _mapper;

        private readonly IMovieNotesService _movieNotesService;

        public MovieNotesAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IMovieNotesService movieNotesService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _movieNotesService = movieNotesService;
        }
		
		public void InsertMovieNotes(MovieNotesModel movieNotes)
		{
			_movieNotesService.InsertMovieNotes(_mapper.Map<MovieNotes>(movieNotes));
		}

		public void UpdateMovieNotes(MovieNotesModel movieNotes)
		{
			_movieNotesService.UpdateMovieNotes(_mapper.Map<MovieNotes>(movieNotes));
		}

		public void DeleteMovieNotes(int movieNoteId)
		{
			_movieNotesService.DeleteMovieNotes(movieNoteId);
		}

		public MovieNotesModel GetMovieNotes(int movieNoteId)
		{
			return _mapper.Map<MovieNotesModel>(_movieNotesService.GetMovieNotes(movieNoteId));
		}

		public List<MovieNotesModel> GetMoviesNotes( )
		{
			return _mapper.Map<List<MovieNotesModel>>(_movieNotesService.GetMoviesNotes());
		}

		public List<MovieNotesModel> GetMovieNotesByMovie(int movieId)
		{
			return _mapper.Map<List<MovieNotesModel>>(_movieNotesService.GetMovieNotesByMovie(movieId));
		}

		public List<MovieNotesModel> GetMovieNotesByUser(int userId)
		{
			return _mapper.Map<List<MovieNotesModel>>(_movieNotesService.GetMovieNotesByUser(userId));
		}
		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_movieNotesService.Dispose();
		}

    }
}