using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Director.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class DirectorAppService : ApplicationService, IDirectorAppService
    {
        private readonly IMapper _mapper;

        private readonly IDirectorService _directorService;

        public DirectorAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IDirectorService directorService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _directorService = directorService;
        }
		
		public DirectorModel InsertDirector(DirectorModel director)
		{
			var directorModel = _directorService.InsertDirector(_mapper.Map<Director>(director));
			return _mapper.Map<DirectorModel>(directorModel);
		}

		public DirectorModel UpdateDirector(DirectorModel director)
		{
			var directorModel =  _directorService.UpdateDirector(_mapper.Map<Director>(director));
			return _mapper.Map<DirectorModel>(directorModel);
		}

		public DirectorModel DeleteDirector(int directorId)
		{
			var directorModel =  _directorService.DeleteDirector(directorId);
			return _mapper.Map<DirectorModel>(directorModel);
		}

		public DirectorModel GetDirector(int directorId)
		{
			return _mapper.Map<DirectorModel>(_directorService.GetDirector(directorId));
		}

		public List<DirectorModel> GetDirectors( )
		{
			return _mapper.Map<List<DirectorModel>>(_directorService.GetDirectors());
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_directorService.Dispose();
		}

    }
}