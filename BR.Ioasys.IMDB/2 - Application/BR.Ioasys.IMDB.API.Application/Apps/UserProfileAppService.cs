using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.UserProfile.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class UserProfileAppService : ApplicationService, IUserProfileAppService
    {
        private readonly IMapper _mapper;

        private readonly IUserProfileService _userProfileService;

        public UserProfileAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IUserProfileService userProfileService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _userProfileService = userProfileService;
        }
		
		public UserProfileModel InsertUserProfile(UserProfileModel userProfile)
		{
			var profileModel =  _userProfileService.InsertUserProfile(_mapper.Map<UserProfile>(userProfile));
			return _mapper.Map<UserProfileModel>(profileModel);
		}

		public UserProfileModel UpdateUserProfile(UserProfileModel userProfile)
		{
			var profileModel = _userProfileService.UpdateUserProfile(_mapper.Map<UserProfile>(userProfile));
			return _mapper.Map<UserProfileModel>(profileModel);
		}

		public UserProfileModel DeleteUserProfile(int profileId)
		{
			var profileModel =  _userProfileService.DeleteUserProfile(profileId);
			return _mapper.Map<UserProfileModel>(profileModel);
		}

		public UserProfileModel GetUserProfile(int profileId)
		{
			return _mapper.Map<UserProfileModel>(_userProfileService.GetUserProfile(profileId));
		}

		public List<UserProfileModel> GetProfiles( )
		{
			return _mapper.Map<List<UserProfileModel>>(_userProfileService.GetProfiles());
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_userProfileService.Dispose();
		}

    }
}