using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Movie.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
	public class MovieAppService : ApplicationService, IMovieAppService
	{
		private readonly IMapper _mapper;

		private readonly IMovieService _movieService;

		public MovieAppService(IMediator mediator,
							   INotificationHandler<DomainNotification> notifications,
							   IMapper mapper,
							   IMovieService movieService
									   ) : base(mediator, notifications)
		{
			_mapper = mapper;
			_movieService = movieService;
		}

		public MovieModel InsertMovie(MovieModel movie)
		{
			var movieModel = _movieService.InsertMovie(_mapper.Map<Movie>(movie));
			return _mapper.Map<MovieModel>(movieModel);
		}

		public MovieModel UpdateMovie(MovieModel movie)
		{
			var movieModel = _movieService.UpdateMovie(_mapper.Map<Movie>(movie));
			return _mapper.Map<MovieModel>(movieModel);
		}

		public MovieModel DeleteMovie(int movieId)
		{
			var movieModel = _movieService.DeleteMovie(movieId);
			return _mapper.Map<MovieModel>(movieModel);
		}

		public MovieModel GetMovie(int movieId)
		{
			return _mapper.Map<MovieModel>(_movieService.GetMovie(movieId));
		}

		public List<MovieModel> GetMovies()
		{
			return _mapper.Map<List<MovieModel>>(_movieService.GetMovies());
		}

		public List<MovieModel> GetMovieByGender(int genderId)
		{
			return _mapper.Map<List<MovieModel>>(_movieService.GetMovieByGender(genderId));
		}

		public List<MovieModel> GetMovieByDirector(int directorId)
		{
			return _mapper.Map<List<MovieModel>>(_movieService.GetMovieByDirector(directorId));
		}

		public List<MovieDetailModel> GetMoviesByFilter(MovieFilterModel movie)
		{
			return _mapper.Map<List<MovieDetailModel>>(_movieService.GetMoviesByFilter(_mapper.Map<Movie>(movie)));
		}

		public MovieDetailModel DetailMovie(int movieId)
		{
			return _mapper.Map<MovieDetailModel>(_movieService.DetailMovie(movieId));
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_movieService.Dispose();
		}

    }
}