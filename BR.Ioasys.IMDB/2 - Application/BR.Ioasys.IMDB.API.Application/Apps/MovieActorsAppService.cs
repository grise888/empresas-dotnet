using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.MovieActors.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class MovieActorsAppService : ApplicationService, IMovieActorsAppService
    {
        private readonly IMapper _mapper;

        private readonly IMovieActorsService _movieActorsService;

        public MovieActorsAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IMovieActorsService movieActorsService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _movieActorsService = movieActorsService;
        }
		
		public void InsertMovieActors(MovieActorsModel movieActors)
		{
			_movieActorsService.InsertMovieActors(_mapper.Map<MovieActors>(movieActors));
		}

		public void UpdateMovieActors(MovieActorsModel movieActors)
		{
			_movieActorsService.UpdateMovieActors(_mapper.Map<MovieActors>(movieActors));
		}

		public void DeleteMovieActors(int movieActorId)
		{
			_movieActorsService.DeleteMovieActors(movieActorId);
		}

		public MovieActorsModel GetMovieActors(int movieActorId)
		{
			return _mapper.Map<MovieActorsModel>(_movieActorsService.GetMovieActors(movieActorId));
		}

		public List<MovieActorsModel> GetMoviesActors( )
		{
			return _mapper.Map<List<MovieActorsModel>>(_movieActorsService.GetMoviesActors());
		}

		public List<MovieActorsModel> GetMovieActorsByActor(int actorId)
		{
			return _mapper.Map<List<MovieActorsModel>>(_movieActorsService.GetMovieActorsByActor(actorId));
		}

		public List<MovieActorsModel> GetMovieActorsByMovie(int movieId)
		{
			return _mapper.Map<List<MovieActorsModel>>(_movieActorsService.GetMovieActorsByMovie(movieId));
		}


		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_movieActorsService.Dispose();
		}

    }
}