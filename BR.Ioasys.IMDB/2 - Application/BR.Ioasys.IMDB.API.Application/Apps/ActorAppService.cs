using AutoMapper;
using BR.Ioasys.IMDB.API.Application.Apps.Core;
using BR.Ioasys.IMDB.API.Application.Interfaces;
using BR.Ioasys.IMDB.API.Application.ViewModels;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor;
using BR.Ioasys.IMDB.API.Domain.Aggregates.Movies.Actor.Services;
using BR.Ioasys.IMDB.API.Domain.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.Apps
{
    public class ActorAppService : ApplicationService, IActorAppService
    {
        private readonly IMapper _mapper;

        private readonly IActorService _actorService;

        public ActorAppService(IMediator mediator,
                               INotificationHandler<DomainNotification> notifications,
                               IMapper mapper,
                               IActorService actorService
                                       ) : base(mediator, notifications)
        {
            _mapper = mapper;
            _actorService = actorService;
        }
		
		public ActorModel InsertActor(ActorModel actor)
		{
			var actorModel = _actorService.InsertActor(_mapper.Map<Actor>(actor));
			return _mapper.Map<ActorModel>(actorModel);
		}

		public ActorModel UpdateActor(ActorModel actor)
		{
			var actorModel =  _actorService.UpdateActor(_mapper.Map<Actor>(actor));
			return _mapper.Map<ActorModel>(actorModel);
		}

		public ActorModel DeleteActor(int actorId)
		{
			var actorModel =  _actorService.DeleteActor(actorId);
			return _mapper.Map<ActorModel>(actorModel);
		}

		public ActorModel GetActor(int actorId)
		{
			return _mapper.Map<ActorModel>(_actorService.GetActor(actorId));
		}

		public List<ActorModel> GetActors( )
		{
			return _mapper.Map<List<ActorModel>>(_actorService.GetActors());
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
			Dispose(true);
		}
		protected virtual void Dispose(bool disposing)
		{

			_actorService.Dispose();
		}

    }
}