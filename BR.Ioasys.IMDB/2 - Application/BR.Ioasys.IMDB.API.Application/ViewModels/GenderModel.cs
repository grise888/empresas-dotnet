using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class GenderModel
    {
		[JsonProperty("genderId")]
		public int GenderId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
		
    }
}