using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class ActorModel
    {
		[JsonProperty("actorId")]
		public int ActorId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
		
    }
}