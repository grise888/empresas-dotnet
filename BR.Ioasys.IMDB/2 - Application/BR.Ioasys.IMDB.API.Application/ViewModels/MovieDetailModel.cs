using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class MovieDetailModel : BaseModel
	{
		[JsonProperty("movieId")]
		public int MovieId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("detail")]
		public string Detail { get; set; }

		[JsonProperty("genderId")]
		public int GenderId { get; set; }

		[JsonProperty("genderName")]
		public string GenderName { get; set; }

		[JsonProperty("directorId")]
		public int DirectorId { get; set; }

		[JsonProperty("directorName")]
		public string DirectorName { get; set; }
		public List<ActorModel> ActorList { get; set; }

	}
}