using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class MovieNotesModel
    {
		[JsonProperty("movieNoteId")]
		public int MovieNoteId { get; set; }

		[JsonProperty("movieId")]
		public int MovieId { get; set; }

		[JsonProperty("userId")]
		public int UserId { get; set; }

		[JsonProperty("note")]
		public int Note { get; set; }
		
    }
}