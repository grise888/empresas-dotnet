using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class UserModel
    {
		[JsonProperty("userId")]
		public int UserId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("login")]
		public string Login { get; set; }

		[JsonProperty("password")]
		public string Password { get; set; }

		[JsonProperty("statusId")]
		public int StatusId { get; set; }

		[JsonProperty("profileId")]
		public int ProfileId { get; set; }
		
    }
}