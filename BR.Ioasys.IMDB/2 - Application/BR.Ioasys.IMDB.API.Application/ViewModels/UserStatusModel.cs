using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class UserStatusModel
    {
		[JsonProperty("statusId")]
		public int StatusId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
		
    }
}