using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class DirectorModel
    {
		[JsonProperty("directorId")]
		public int DirectorId { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		
    }
}