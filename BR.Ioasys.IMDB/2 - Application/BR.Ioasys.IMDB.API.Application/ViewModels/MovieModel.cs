using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class MovieModel : BaseModel
	{
		
		public int MovieId { get; set; }
		public string Name { get; set; }
		public string Detail { get; set; }
    	public int GenderId { get; set; }
		public int DirectorId { get; set; }

	}
}