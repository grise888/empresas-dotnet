using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class MovieActorsModel
    {

		[JsonProperty("movieActorId")]
		public int MovieActorId { get; set; }
		[JsonProperty("movieId")]
		public int MovieId { get; set; }
		[JsonProperty("actorId")]
		public int ActorId { get; set; }
		
    }
}