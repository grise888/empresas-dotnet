using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class UserProfileModel
    {
		[JsonProperty("profileId")]
		public int ProfileId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }
		
    }
}