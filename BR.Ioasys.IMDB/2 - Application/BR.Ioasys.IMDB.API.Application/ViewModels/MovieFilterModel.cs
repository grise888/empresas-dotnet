using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
	public class MovieFilterModel : BaseModel
	{
		[JsonProperty("movieId")]
		public int MovieId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("genderName")]
		public string GenderName { get; set; }

		[JsonProperty("directorName")]
		public string DirectorName { get; set; }

		[JsonProperty("actorName")]
		public string ActorName { get; set; }

	}
}