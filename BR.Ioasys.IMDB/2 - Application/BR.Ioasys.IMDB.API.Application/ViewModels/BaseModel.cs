﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.Ioasys.IMDB.API.Application.ViewModels
{
    public class BaseModel
    {
        [JsonProperty("pagination")]
        public bool Pagination { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("numberRecords")]
        public int NumberRecords { get; set; }
    }
}
